package fr.afpa.control;

import fr.afpa.metier.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;
/*
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
*/
import fr.afpa.beans.*;
import fr.afpa.metier.*;

public class Control {

	// Pour stocker les objets des fichiers :
	private static final String PATH_QR = "qrcode.jpg";
	private static ArrayList<Client> listClient;
	private static ArrayList<Conseiller> listConseiller;
	private static ArrayList<Admin> listAdmin;
	private static ArrayList<Agence> listAgence;
	private static ArrayList<CompteBancaire> listComptesBancaire;
	
	private static final String CLIENT_STR = "Client";
	private static final String CONSEILLER_STR = "Conseiller";
	private static final String ADMIN_STR = "Admin";
	
	private static String connected;
	private static Client client;
	private static Conseiller conseiller;
	private static Admin admin;
	private static Agence agence;
	
	private static int verifCodeAgence;

	public static void lancementApp() {

		listClient = new ArrayList<Client>();
		listConseiller = new ArrayList<Conseiller>();
		listAdmin = new ArrayList<Admin>();
		listAgence = new ArrayList<Agence>();
		listComptesBancaire = new ArrayList<CompteBancaire>();
		
		GestionAdmin.ecrireFichier(listClient, GestionAdmin.getClientPath());
		GestionAdmin.ecrireFichier(listConseiller, GestionAdmin.getConseillerPath());
		GestionAdmin.ecrireFichier(listAdmin, GestionAdmin.getAdminPath());
		GestionAdmin.ecrireFichier(listAgence, GestionAdmin.getAgencePath());
		GestionAdmin.ecrireFichier(listComptesBancaire, GestionAdmin.getCompteBancairePath());
	}

	public static boolean controlFormatNom(String nom) {
		for (int i = 0; i < nom.length(); i++) {
			if (!(Character.isAlphabetic(nom.charAt(i)))) {
				return false;
			}
		}
		return true;
	}

	public static boolean controlFormatMail(String mail) {
		if (!(mail.contains("@"))) {
			return false;
		}
		return true;
	}

	public static boolean controlFormatDate(String date) {

		// Format : 1999-09-09

		if (date.length() != 10) {
			return false;
		}

		if (date.charAt(4) != '-' || date.charAt(7) != '-') {
			return false;
		}

		for (int i = 0; i < 4; i++) {
			if (!(Character.isDigit(date.charAt(i)))) {
				return false;
			}
		}
		for (int i = 5; i < 7; i++) {
			if (!(Character.isDigit(date.charAt(i)))) {
				return false;
			}
		}
		for (int i = 8; i < date.length(); i++) {
			if (!(Character.isDigit(date.charAt(i)))) {
				return false;
			}
		}

		return true;
	}

	public static boolean controlFormatCodeClient(String code) {

		// AA333333

		if (code.length() != 8) {
			return false;
		}

		if (!(Character.isAlphabetic(code.charAt(0))) || !(Character.isAlphabetic(code.charAt(1)))) {
			return false;
		}

		for (int i = 2; i < code.length(); i++) {
			if (!(Character.isDigit(code.charAt(i)))) {
				return false;
			}
		}

		return true;
	}
	
	// Control du format Client
	
	public static boolean controlFormatClient(String nom, String prenom, String email, String login, String codeClient,
			String dateNaiss) {

		boolean verifNom = controlFormatNom(nom);
		boolean verifPrenom = controlFormatNom(prenom);
		boolean verifMail = controlFormatMail(email);
		boolean verifLog = controlFormatIdClient(login);
		boolean verifCodeClient = controlFormatCodeClient(codeClient);
		boolean verifDate = controlFormatDate(dateNaiss);

		if (verifNom && verifPrenom && verifMail && verifLog && verifCodeClient && verifDate) {
			return true;
		}
		return false;
	}
	

	public static boolean modifierClient(String nom, String prenom, String email, String login, String codeClient,
			String dateNaiss) {
		if (controlFormatClient(nom, prenom, email, login, codeClient, dateNaiss)) {
			GestionConseiller.modifierClient(nom, prenom, email, login, codeClient, dateNaiss);
			return true;
		}
		return false;
	}
	
	public static boolean ajoutClient(String nom, String prenom, String email, String login, String codeClient,
			String dateNaiss) {
		if (controlFormatClient(nom, prenom, email, login, codeClient, dateNaiss)) {
			GestionConseiller.ajoutClient(nom, prenom, email, login, codeClient, dateNaiss);
			return true;
		}
		return false;
	}
	
	public static void supprimerClient(Client client) {
			GestionClient.supprimerClient(client);
	}
	
	// Control du format Conseiller
	
	public static boolean controlFormatConseiller(String nom, String prenom, String email, String login) {

		boolean verifNom = controlFormatNom(nom);
		boolean verifPrenom = controlFormatNom(prenom);
		boolean verifMail = controlFormatMail(email);
		boolean verifLog = controlFormatIdConseiller(login);


		if (verifNom && verifPrenom && verifMail && verifLog) {
			return true;
		}
		return false;
	}
	
	public static boolean ajoutConseiller(String nom, String prenom, String mail, String login) {
		if (controlFormatConseiller(nom, prenom, mail, login)) {
			GestionAdmin.ajoutConseiller(nom, prenom, mail, login);
			return true;
		}
		return false;
	}
	
	public static boolean modifierConseiller(String nom, String prenom, String email, String login) {
		if (controlFormatConseiller(nom, prenom, email, login)) {
			GestionConseiller.modifierConseiller(nom, prenom, email, login);
			return true;
		}
		return false;
	}
	

	public static void supprimerConseiller(Conseiller conseiller) {
		GestionConseiller.supprimerConseiller(conseiller);
		
	}

	
	// Cr�er une agence
	
	public static boolean creerAgence(String code, String nom, String numRue, String nomRue, String ville) {
			if (controlFormatCodeAgence(code)) {
				GestionAdmin.creerAgence(code, nom, numRue, nomRue, ville);
				return true;
			} else {
				System.out.println("C pa bon");
				return false;
			}
		}
	
	// Control du format CodeAgence
	
	public static boolean controlFormatCodeAgence(String code) {
		if (code.length() != 3) {
			return false;
		} else {
			for (int i = 0; i < code.length(); i++) {
				if (!(Character.isDigit(code.charAt(i)))) {
					return false;
				}
			}
		}

		return true;
	}

	public static ArrayList<Agence> controlCodeAgenceFichier(String code) {

		boolean verifFormatCode = controlFormatCodeAgence(code);

		if (verifFormatCode) {
			FileInputStream f = null;
			ObjectInputStream ois = null;

			try {

				f = new FileInputStream(GestionClient.getAgencePath());
				ois = new ObjectInputStream(f);
				listAgence = new ArrayList<Agence>();
				listAgence.clear();

				listAgence = (ArrayList<Agence>) ois.readObject();

			} catch (EOFException e) {

				try {
					ois.close();
					f.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

			} catch (Exception e1) {
				e1.printStackTrace();
			} finally {
				if (ois != null) {
					try {
						ois.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			verifCodeAgence = checkCodeAgence(code, listAgence);
			if (verifCodeAgence != -1) {
				return listAgence;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public static int getVerifCodeAgence() {
		return verifCodeAgence;
	}


	public static int checkCodeAgence(String code, ArrayList<Agence> listAgences) {
		for (int i = 0; i < listAgences.size(); i++) {
			if (code.equals(String.valueOf(listAgences.get(i).getCode()))) {
				agence = listAgences.get(i);

				return i;
			}
		}
		return -1;
	}


	// Changer la Domiciliation
	
	public static void changerDomicile(String numRue, String nomRue, String ville, ArrayList<Agence> listAgence) {
		GestionConseiller.modifierDomiciliationClient(numRue, nomRue, ville, listAgence);
	}

	
	// Login Client

	public static boolean controlFormatIdClient(String id) {

		if (id.length() != 10) {
			return false;
		} else {
			for (int i = 0; i < id.length(); i++) {
				if (!(Character.isDigit(id.charAt(i)))) {
					return false;
				}
			}
		}

		return true;
	}

	public static Client controlIdClient(String id) {

		boolean verifFormatId = controlFormatIdClient(id);
		

		if (verifFormatId) {

			listClient = GestionAdmin.lireFichierClient();

			Client verifIdClient = checkIdClient(id, listClient);

			listClient.clear();
			System.out.println("ok");
			if (verifIdClient != null) {
				return verifIdClient;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public static Client checkIdClient(String id, ArrayList<Client> listClient) {
		for (int i = 0; i < listClient.size(); i++) {
			if (id.equals(String.valueOf(listClient.get(i).getLogin()))) {
				setClient(listClient.get(i));

				return listClient.get(i);
			}
		}
		return null;
	}

	
	// Login Conseiller

	public static boolean controlFormatIdConseiller(String id) {

		if (id.length() != 6) {
			return false;
		} else {
			
			if (id.charAt(0) != 67 && id.charAt(1) != 79) {
				return false;
			}
			
			for (int i = 2; i < id.length(); i++) {
				if (!(Character.isDigit(id.charAt(i)))) {
					return false;
				}
			}
		}

		return true;
	}

	public static Conseiller controlIdConseiller(String id) {

		boolean verifFormatId = controlFormatIdConseiller(id);


		if (verifFormatId) {

			listConseiller = GestionAdmin.lireFichierConseiller();

			Conseiller verifIdConseiller = checkIdConseiller(id, listConseiller);


			System.out.println("ok");


			if (verifIdConseiller != null) {
				return verifIdConseiller;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public static Conseiller checkIdConseiller(String id, ArrayList<Conseiller> listConseiller) {
		for (int i = 0; i < listConseiller.size(); i++) {
			if (id.equals(String.valueOf(listConseiller.get(i).getLogin()))) {
				setConseiller(listConseiller.get(i));
				return listConseiller.get(i);
			}
		}
		return null;
	}

	
	//Login Client Input

	public static Client controlIdClientInput(String id) {

		boolean verifFormatId = controlFormatIdClient(id);

		if (verifFormatId) {
			FileInputStream f = null;
			ObjectInputStream ois = null;

			try {

				f = new FileInputStream(GestionClient.getClientPath());
				ois = new ObjectInputStream(f);
				listClient = new ArrayList<Client>();
				listClient.clear();

				listClient = (ArrayList<Client>) ois.readObject();
				
				ois.close();
				f.close();

			} catch (EOFException e) {

				try {
					ois.close();
					f.close();
					
				} catch (IOException e1) {
					e1.printStackTrace();
				}

			}catch (Exception e1) {
				e1.printStackTrace();
			} finally {
				if (ois != null) {
					try {
						ois.close();
						f.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			Client verifIdClient = checkIdClientInput(id, listClient);

			if (verifIdClient != null) {
				return verifIdClient;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public static Client checkIdClientInput(String id, ArrayList<Client> listClient) {
		for (int i = 0; i < listClient.size(); i++) {
			if (id.equals(String.valueOf(listClient.get(i).getLogin()))) {
				return listClient.get(i);
			}
		}
		return null;
	}
	
	
	// Login Administrateur
	
	public static boolean controlFormatIdAdmin(String id) {

		if (id.length() != 5) {
			return false;
		} else {
			if (id.charAt(0) != 65 && id.charAt(1) != 68 && id.charAt(2) == 77 ) {
				return false;
			}
			
			for (int i = 3; i < id.length(); i++) {
				if (!(Character.isDigit(id.charAt(i)))) {
					return false;
				}
			}
		}

		return true;
	}

	public static Admin controlIdAdmin(String id) {

		boolean verifFormatId = controlFormatIdConseiller(id);

		if (verifFormatId) {

			listAdmin = GestionAdmin.lireFichierAdmin();

			Admin verifIdAdmin = checkIdAdmin(id, listAdmin);

			if (verifIdAdmin != null) {
				return verifIdAdmin;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public static Admin checkIdAdmin(String id, ArrayList<Admin> listAdmin) {
		for (int i = 0; i < listAdmin.size(); i++) {
			if (id.equals(String.valueOf(listConseiller.get(i).getLogin()))) {
				setAdmin(listAdmin.get(i));
				return listAdmin.get(i);
			}
		}
		return null;
	}
	
	/*
	//QR Code
	
	public static void genererQRCode() {
		
		String qr = "";
		
		if (connected.equals(CLIENT_STR)) {
			qr = qr + "BEGIN:VCARD\r\n"
					+ "FN:" + getClient().getNom() + " " + getClient().getPrenom() + "\r\n"
					+ "EMAIL:" + getClient().getEmail() + "\r\n"
					+ "CODE_CLIENT:" + getClient().getCodeClient() + "\r\n"
					+ "LOGIN:" + getClient().getLogin() + "\r\n";
		}else if (connected.equals(CONSEILLER_STR)) {
			qr = qr + "BEGIN:VCARD\r\n"
					+ "FN:" + getConseiller().getNom() + " " + getConseiller().getPrenom() + "\r\n"
					+ "EMAIL:" + getConseiller().getEmail() + "\r\n"
					+ "LOGIN:" + getConseiller().getLogin() + "\r\n";
		}else if (connected.equals(ADMIN_STR)) {
			qr = qr + "BEGIN:VCARD\r\n"
					+ "FN:" + getAdmin().getNom() + " " + getAdmin().getPrenom() + "\r\n"
					+ "EMAIL:" + getAdmin().getEmail() + "\r\n"
					+ "LOGIN:" + getAdmin().getLogin() + "\r\n";
		}
		
		
		BitMatrix matrix;
		try {
			matrix = new MultiFormatWriter().encode(qr, BarcodeFormat.QR_CODE, 300, 300);
			MatrixToImageWriter.writeToPath(matrix, "jpg", Paths.get(PATH_QR));
		} catch (Exception e) {
			System.out.println("pb");
		}
	}
*/
	
	
	// Control num�ro de compte
	
	public static boolean controlFormatNumeroCompte(String numCompte) {
			
			if (numCompte.length() != 11) {
				return false;
			} else {
				for (int i = 0; i < numCompte.length(); i++) {
					if (!(Character.isDigit(numCompte.charAt(i)))) {
						return false;
					}
				}
			}
			
			return true;
		}
	public static boolean controlFormatSolde(String sold) {
		for (int i = 0; i < sold.length(); i++) {
			if (!(Character.isDigit(sold.charAt(i)))) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean controlFormatDecouvert(String decouvert) {
		
		if (decouvert.length() != 1) {
			return false;
		} else {
			if (!(Character.isAlphabetic(decouvert.charAt(0)))) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean ajoutCompteBancaire(String codeAgence, String numCompte, String solde, String type, String client,String decouvert) {
		if (controlFormatNumeroCompte(numCompte)
				&& controlFormatCodeAgence(codeAgence)
				&& controlFormatIdClient(client)
				&& controlFormatSolde(solde)
				&& controlFormatDecouvert(decouvert)) {
			
			listClient = GestionAdmin.lireFichierClient();
			listAgence = GestionAdmin.lireFichierAgence();
			
			if (checkCodeAgence(codeAgence, listAgence) != -1
					&& checkIdClient(client, listClient) != null) {
				
				Control.client = checkIdClient(client, listClient);
				Client newClient = GestionAdmin.ajoutCompteBancaire(numCompte, Control.client, Control.agence, solde, decouvert);

				listClient = GestionAdmin.lireFichierClient();
				
				for (int i = 0; i < listClient.size(); i++) {
					if (listClient.get(i).getLogin().equals(newClient.getLogin())) {
						listClient.remove(i);
						listClient.add(newClient);
					}
				}
				GestionAdmin.ecrireFichier(listClient, GestionClient.getClientPath());
				
				return true;
				
			}
		}
		return false;
	}
	
	
	/*public static Client checkNumeroCompte(String numCompte, ArrayList<Client> listClient) {
		for (int i = 0; i < listClient.size(); i++) {
			if (numCompte.equals(String.valueOf(listClient.get(i).//METTRE NUMERO DU COMPTE))) {
				return listClient.get(i);
			}
		}
		return null;
	}*/
	
	//Getter And Setter
	
	public static String getConnected() {
		return connected;
	}

	public static void setConnected(String connected) {
		Control.connected = connected;
	}

	public static String getClientStr() {
		return CLIENT_STR;
	}

	public static String getConseillerStr() {
		return CONSEILLER_STR;
	}

	public static String getAdminStr() {
		return ADMIN_STR;
	}

	public static Client getClient() {
		return client;
	}

	public static void setClient(Client client) {
		Control.client = client;
	}

	public static Admin getAdmin() {
		return admin;
	}

	public static void setAdmin(Admin admin) {
		Control.admin = admin;
	}

	public static Conseiller getConseiller() {
		return conseiller;
	}

	public static void setConseiller(Conseiller conseiller) {
		Control.conseiller = conseiller;
	}

	public static String getPathQr() {
		return PATH_QR;
	}

	
	
	
	
	
}
