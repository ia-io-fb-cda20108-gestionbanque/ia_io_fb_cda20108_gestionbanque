package fr.afpa.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Operations extends JFrame {
	private JFrame frame;
	private ImageIcon icon;
	private JLabel cDABanque;
	
	private JPanel panelFrame;
	private JPanel panelBoutton;
	
	private JTextField demandeID;
	private Container content;
	private GridBagConstraints gbc;
	private JButton valider, effacer;

	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(40, 50, 50, 20);
		
		//Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		//Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelFrame.add(panelBoutton, gbc);
		
		// Titre
		cDABanque = new JLabel("Entrez le num�ro du compte");
		cDABanque.setFont(new Font("Century Gothic", Font.BOLD, 60));
		cDABanque.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(cDABanque, gbc);

		// Entrez l ID
		demandeID = new JTextField("Num�ro du compte � consulter", 10);
		demandeID.setFont(new Font("Century Gothic", Font.BOLD, 40));
		demandeID.setHorizontalAlignment(JTextField.CENTER); 
		demandeID.setForeground(new Color(205, 205, 205));
		demandeID.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(demandeID, gbc);
		
		//Button Valider
		valider = new JButton("Valider");
		valider.setFont(new Font("Century Gothic", Font.BOLD, 30));
		valider.setForeground(Color.white);
		valider.setBackground(new Color(37, 41, 58));
		valider.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelBoutton.add(valider);

		//Button Effacer
		effacer = new JButton("Effacer");
		effacer.setFont(new Font("Century Gothic", Font.BOLD, 30));
		effacer.setForeground(new Color(37, 41, 58));
		effacer.setBackground(Color.white);
		effacer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelBoutton.add(effacer);
		
		//Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

}
