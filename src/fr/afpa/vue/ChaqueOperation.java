package fr.afpa.vue;

import java.awt.*;
import javax.swing.*;

public class ChaqueOperation extends JFrame{
	private JFrame frame;
	private ImageIcon icon;
	
	private JPanel panelFrame;
	private JPanel panelBoutton;

	private Container content;
	private GridBagConstraints gbc;
	private JLabel infoLabel;

	private JLabel nom1;
	private JLabel nom2;
	private JLabel nom3;
	private JLabel operation1;
	private JLabel operation2;
	private JLabel operation3;
	private JPanel infoPanel1;
	private JPanel infoPanel2;
	private JPanel infoPanel3;
	private JTextArea allInfos1;
	private JTextArea allInfos2;
	private JTextArea allInfos3;


	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		
		gbc.insets = new Insets(30, 30, 30, 30);
		
		//Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		//Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(panelBoutton, gbc);
		
		// Titre
		infoLabel = new JLabel("Voici les opérations du compte");
		infoLabel.setFont(new Font("Century Gothic", Font.BOLD, 75));
		infoLabel.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 50;
		panelFrame.add(infoLabel, gbc);

		
		// Affichage infos1
		allInfos1 = new JTextArea(3,30);
		allInfos1.setFont(new Font("Century Gothic", Font.ITALIC, 30));
		allInfos1.setForeground(new Color(37, 41, 58));
		allInfos1.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		allInfos1.setBackground(new Color(255,255,255));
		allInfos1.setEditable(false);
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 50;
		
		// Dans TextArea
		infoPanel1 = new JPanel(new FlowLayout());
		infoPanel1.setBackground(new Color(0, 0, 0, 0));

		// Ligne d'info NOM
		nom1 = new JLabel("Operation 1 : ");
		nom1.setFont(new Font("Century Gothic", Font.ITALIC, 30));
		infoPanel1.add(nom1);
		
		operation1 = new JLabel(" ");
		operation1.setFont(new Font("Century Gothic", Font.ITALIC, 30));
		infoPanel1.add(operation1);

		
		gbc.gridx = 0;
		gbc.gridy = 1;		
		
		panelFrame.add(infoPanel1, gbc);
		panelFrame.add(allInfos1, gbc);
		
		
		// Affichage infos2
		allInfos2 = new JTextArea(3,30);
		allInfos2.setFont(new Font("Century Gothic", Font.ITALIC, 30));
		allInfos2.setForeground(new Color(37, 41, 58));
		allInfos2.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		allInfos2.setBackground(new Color(255,255,255));
		allInfos2.setEditable(false);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 50;
		
		
		// Dans TextArea
		
		infoPanel2 = new JPanel(new FlowLayout());
		infoPanel2.setBackground(new Color(0, 0, 0, 0));

		// Ligne d'info NOM
		nom2 = new JLabel("Operation 2 : ");
		nom2.setFont(new Font("Century Gothic", Font.ITALIC, 30));
		infoPanel2.add(nom2);
		
		operation2 = new JLabel(" ");
		operation2.setFont(new Font("Century Gothic", Font.ITALIC, 30));
		infoPanel2.add(operation2);

				
		gbc.gridx = 0;
		gbc.gridy = 2;		
		
		panelFrame.add(infoPanel2, gbc);
		panelFrame.add(allInfos2, gbc);
		
		// Affichage infos3
		allInfos3 = new JTextArea(3,30);
		allInfos3.setFont(new Font("Century Gothic", Font.ITALIC, 30));
		allInfos3.setForeground(new Color(37, 41, 58));
		allInfos3.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		allInfos3.setBackground(new Color(255,255,255));
		allInfos3.setEditable(false);
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 50;
		
		
		// Dans TextArea
		
		infoPanel3 = new JPanel(new FlowLayout());
		infoPanel3.setBackground(new Color(0, 0, 0, 0));

		// Ligne d'info NOM
		nom3 = new JLabel("Operation 3 : ");
		nom3.setFont(new Font("Century Gothic", Font.ITALIC, 30));
		infoPanel3.add(nom3);
		
		operation3 = new JLabel(" ");
		operation3.setFont(new Font("Century Gothic", Font.ITALIC, 30));
		infoPanel3.add(operation3);
	
		
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		
		panelFrame.add(infoPanel3, gbc);
		panelFrame.add(allInfos3, gbc);
				
		
		// Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}
}
