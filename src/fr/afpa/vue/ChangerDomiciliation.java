package fr.afpa.vue;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.util.ArrayList;

import javax.swing.*;

import fr.afpa.beans.Agence;
import fr.afpa.control.Control;


public class ChangerDomiciliation extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private ImageIcon icon;
	private JLabel titre;
	
	private JPanel panelFrame;
	private JPanel panelBoutton;
	
	private JTextField demande;
	private Container content;
	private GridBagConstraints gbc;
	private JButton valider, effacer, enregistrer;

	private JTextField demandeNumero;
	private JTextField demandeNomRue;
	private JTextField demandeVille;
	private ArrayList<Agence> verifAgence;

	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);
		
		//Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		//Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelFrame.add(panelBoutton, gbc);
		
		// Titre
		titre = new JLabel("Entrez le code Agence");
		titre.setFont(new Font("Century Gothic", Font.BOLD, 60));
		titre.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(titre, gbc);

		// Entrez l ID
		demande = new JTextField("Code Agence", 10);
		demande.setFont(new Font("Century Gothic", Font.ITALIC, 40));
		demande.setHorizontalAlignment(JTextField.CENTER); 
		demande.setForeground(new Color(37, 41, 58));
		demande.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(demande, gbc);
		
		//Button Valider
		valider = new JButton("Valider");
		valider.setFont(new Font("Century Gothic", Font.BOLD, 30));
		valider.setForeground(Color.white);
		valider.setBackground(new Color(37, 41, 58));
		valider.setCursor(new Cursor(Cursor.HAND_CURSOR));
		valider.addActionListener(this);
		panelBoutton.add(valider);

		//Button Effacer
		effacer = new JButton("Effacer");
		effacer.setFont(new Font("Century Gothic", Font.BOLD, 30));
		effacer.setForeground(new Color(37, 41, 58));
		effacer.setBackground(Color.white);
		effacer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		effacer.addActionListener(this);
		panelBoutton.add(effacer);
		
		//Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}
	
	public void visuelSuite() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);
		
		//Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		//Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelFrame.add(panelBoutton, gbc);
		
		// Titre
		titre = new JLabel("Agence");
		titre.setFont(new Font("Century Gothic", Font.BOLD, 60));
		titre.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(titre, gbc);

		// Entrez le numero de rue
		demandeNumero = new JTextField("Num�ro de rue", 10);
		demandeNumero.setFont(new Font("Century Gothic",Font.BOLD, 40));
		demandeNumero.setHorizontalAlignment(JTextField.CENTER); 
		demandeNumero.setForeground(new Color(205, 205, 205));
		demandeNumero.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 1;
		panelFrame.add(demandeNumero, gbc);
		
		// Entrez la rue
		demandeNomRue = new JTextField("Nom de rue", 10);
		demandeNomRue.setFont(new Font("Century Gothic", Font.BOLD, 40));
		demandeNomRue.setHorizontalAlignment(JTextField.CENTER); 
		demandeNomRue.setForeground(new Color(205, 205, 205));
		demandeNomRue.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(demandeNomRue, gbc);
		
		// Entrez la ville
		demandeVille = new JTextField("Ville", 10);
		demandeVille.setFont(new Font("Century Gothic", Font.BOLD, 40));
		demandeVille.setHorizontalAlignment(JTextField.CENTER); 
		demandeVille.setForeground(new Color(205, 205, 205));
		demandeVille.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 3;
		panelFrame.add(demandeVille, gbc);
		
		//Button Enregistrer
		enregistrer = new JButton("Enregistrer");
		enregistrer.setFont(new Font("Century Gothic", Font.BOLD, 30));
		enregistrer.setForeground(Color.white);
		enregistrer.setBackground(new Color(37, 41, 58));
		enregistrer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelBoutton.add(enregistrer);

		
		//Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(effacer)) {
			demande.setText("");
		}
		if (e.getSource().equals(valider)) {
			verifAgence = Control.controlCodeAgenceFichier(demande.getText());
			if (verifAgence != null) {
				frame.setVisible(false);
				visuelSuite();
			}
		}
		if (e.getSource().equals(enregistrer)) {
			Control.changerDomicile(
					demandeNumero.getText(),
					demandeNomRue.getText(),
					demandeVille.getText(),
					verifAgence);
		}
		
	}
	
}
