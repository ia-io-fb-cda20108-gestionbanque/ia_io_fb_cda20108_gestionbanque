package fr.afpa.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.afpa.control.Control;

@SuppressWarnings("serial")
public class MenuAdmin extends JFrame implements ActionListener {
	private JFrame frame;
	private ImageIcon icon;
	private JLabel titre;

	private JPanel panelFrame;
	private JPanel panelBoutton;

	private Container content;
	private GridBagConstraints gbc;
	private JButton consulterInfos;
	private JButton consulterComptes;
	private JButton consulterOperation;
	private JButton virement;
	private JButton imprimerReleve;
	private JButton alimenterCompte;
	private JButton retirerArgent;
	private JButton gererComptes;
	private JButton changerDomiciliation;
	private JButton gererClient;
	private JButton gererConseiller;
	private JButton activerCompte;
	private JButton activerClient;
	private JButton creerAgence;
	private JButton boutonRetour;

	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);

		// Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		// Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.CENTER;
		panelFrame.add(panelBoutton, gbc);

		// Titre
		titre = new JLabel("Menu Administrateur");
		titre.setFont(new Font("Century Gothic", Font.BOLD, 60));
		titre.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.CENTER;
		panelFrame.add(titre, gbc);

		// Button ConsulterInfos
		consulterInfos = new JButton("Consulter vos Informations");
		consulterInfos.setFont(new Font("Century Gothic", Font.BOLD, 30));
		consulterInfos.setForeground(new Color(37, 41, 58));
		consulterInfos.setBackground(Color.white);
		consulterInfos.setCursor(new Cursor(Cursor.HAND_CURSOR));
		consulterInfos.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panelFrame.add(consulterInfos, gbc);

		// Entrez ConsulterComptes
		consulterComptes = new JButton("Consulter vos Comptes");
		consulterComptes.setFont(new Font("Century Gothic", Font.BOLD, 30));
		consulterComptes.setForeground(new Color(37, 41, 58));
		consulterComptes.setBackground(Color.white);
		consulterComptes.setCursor(new Cursor(Cursor.HAND_CURSOR));
		consulterComptes.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(consulterComptes, gbc);

		// Entrez la ville
		consulterOperation = new JButton("Consulter vos Op�rations");
		consulterOperation.setFont(new Font("Century Gothic", Font.BOLD, 30));
		consulterOperation.setForeground(new Color(37, 41, 58));
		consulterOperation.setBackground(Color.white);
		consulterOperation.setCursor(new Cursor(Cursor.HAND_CURSOR));
		consulterOperation.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 3;
		panelFrame.add(consulterOperation, gbc);

		// Entrez la ville
		virement = new JButton("Faire un Virement");
		virement.setFont(new Font("Century Gothic", Font.BOLD, 30));
		virement.setForeground(new Color(37, 41, 58));
		virement.setBackground(Color.white);
		virement.setCursor(new Cursor(Cursor.HAND_CURSOR));
		virement.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelFrame.add(virement, gbc);

		// Entrez la ville
		imprimerReleve = new JButton("Imprimer un relev�");
		imprimerReleve.setFont(new Font("Century Gothic", Font.BOLD, 30));
		imprimerReleve.setForeground(new Color(37, 41, 58));
		imprimerReleve.setBackground(Color.white);
		imprimerReleve.setCursor(new Cursor(Cursor.HAND_CURSOR));
		imprimerReleve.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 5;
		panelFrame.add(imprimerReleve, gbc);

		// Entrez la ville
		alimenterCompte = new JButton("Alimenter Compte");
		alimenterCompte.setFont(new Font("Century Gothic", Font.BOLD, 30));
		alimenterCompte.setForeground(new Color(37, 41, 58));
		alimenterCompte.setBackground(Color.white);
		alimenterCompte.setCursor(new Cursor(Cursor.HAND_CURSOR));
		alimenterCompte.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 6;
		panelFrame.add(alimenterCompte, gbc);

		// Entrez la ville
		retirerArgent = new JButton("Retirer Argent");
		retirerArgent.setFont(new Font("Century Gothic", Font.BOLD, 30));
		retirerArgent.setForeground(new Color(37, 41, 58));
		retirerArgent.setBackground(Color.white);
		retirerArgent.setCursor(new Cursor(Cursor.HAND_CURSOR));
		retirerArgent.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 7;
		panelFrame.add(retirerArgent, gbc);

		// Entrez la ville
		gererComptes = new JButton("G�rer les comptes");
		gererComptes.setFont(new Font("Century Gothic", Font.BOLD, 30));
		gererComptes.setForeground(new Color(37, 41, 58));
		gererComptes.setBackground(Color.white);
		gererComptes.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gererComptes.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 1;
		panelFrame.add(gererComptes, gbc);

		// Entrez la ville
		changerDomiciliation = new JButton("Changer domiciliation client");
		changerDomiciliation.setFont(new Font("Century Gothic", Font.BOLD, 30));
		changerDomiciliation.setForeground(new Color(37, 41, 58));
		changerDomiciliation.setBackground(Color.white);
		changerDomiciliation.setCursor(new Cursor(Cursor.HAND_CURSOR));
		changerDomiciliation.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 3;
		panelFrame.add(changerDomiciliation, gbc);

		// Entrez la ville
		gererClient = new JButton("Gerer clients");
		gererClient.setFont(new Font("Century Gothic", Font.BOLD, 30));
		gererClient.setForeground(new Color(37, 41, 58));
		gererClient.setBackground(Color.white);
		gererClient.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gererClient.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 4;
		panelFrame.add(gererClient, gbc);

		// Entrez la ville
		creerAgence = new JButton("Cr�er Agence");
		creerAgence.setFont(new Font("Century Gothic", Font.BOLD, 30));
		creerAgence.setForeground(new Color(37, 41, 58));
		creerAgence.setBackground(Color.white);
		creerAgence.setCursor(new Cursor(Cursor.HAND_CURSOR));
		creerAgence.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 5;
		panelFrame.add(creerAgence, gbc);

		// Entrez la ville
		activerClient = new JButton("Activer/Desactiver Client");
		activerClient.setFont(new Font("Century Gothic", Font.BOLD, 30));
		activerClient.setForeground(new Color(37, 41, 58));
		activerClient.setBackground(Color.white);
		activerClient.setCursor(new Cursor(Cursor.HAND_CURSOR));
		activerClient.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 6;
		panelFrame.add(activerClient, gbc);

		// Entrez la ville
		activerCompte = new JButton("Activer/Desactiver Compte");
		activerCompte.setFont(new Font("Century Gothic", Font.BOLD, 30));
		activerCompte.setForeground(new Color(37, 41, 58));
		activerCompte.setBackground(Color.white);
		activerCompte.setCursor(new Cursor(Cursor.HAND_CURSOR));
		activerCompte.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 7;
		panelFrame.add(activerCompte, gbc);

		// Entrez la ville
		gererConseiller = new JButton("G�rer les Conseillers");
		gererConseiller.setFont(new Font("Century Gothic", Font.BOLD, 30));
		gererConseiller.setForeground(new Color(37, 41, 58));
		gererConseiller.setBackground(Color.white);
		gererConseiller.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gererConseiller.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panelFrame.add(gererConseiller, gbc);
		
		boutonRetour = new JButton("Retour");
		boutonRetour.setPreferredSize(new Dimension(300, 50));
		boutonRetour.setFont(new Font("Century Gothic", Font.BOLD, 30));
		boutonRetour.setForeground(Color.white);
		boutonRetour.setBackground(new Color(37, 41, 58));
		boutonRetour.addActionListener(this);
		boutonRetour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gbc.gridx = 1;
		gbc.gridy = 8;
		panelFrame.add(boutonRetour, gbc);

		// Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(consulterInfos)) {

			frame.setVisible(false);

			ConsulterInformationsAdmin info = new ConsulterInformationsAdmin();
			info.visuel();

		} else if (e.getSource().equals(gererComptes)) {

			frame.setVisible(false);

			GererCompteAdmin gererCompteAdmin = new GererCompteAdmin();
			gererCompteAdmin.visuel();

		} else if (e.getSource().equals(consulterComptes)) {

			frame.setVisible(false);

			VosComptes comptes = new VosComptes();
			comptes.visuel();

		} else if (e.getSource().equals(consulterOperation)) {

			frame.setVisible(false);

			Operations operation = new Operations();
			operation.visuel();

		} else if (e.getSource().equals(changerDomiciliation)) {

			frame.setVisible(false);

			ChangerDomiciliation changerDomiciliation = new ChangerDomiciliation();
			changerDomiciliation.visuel();

		} else if (e.getSource().equals(virement)) {

			frame.setVisible(false);

			FaireVirement virement = new FaireVirement();
			virement.visuel();

		} else if (e.getSource().equals(gererClient)) {

			frame.setVisible(false);

			ModifSuppClient modifSuppClient = new ModifSuppClient();
			modifSuppClient.visuel();

		} else if (e.getSource().equals(imprimerReleve)) {

			frame.setVisible(false);

			System.out.println("PDF");

		} else if (e.getSource().equals(creerAgence)) {

			frame.setVisible(false);

			CreerAgence creerAgence = new CreerAgence();
			creerAgence.visuel();

		} else if (e.getSource().equals(alimenterCompte)) {

			frame.setVisible(false);

			AlimenterCompte alimenterCompte = new AlimenterCompte();
			alimenterCompte.visuel();

		} else if (e.getSource().equals(activerClient)) {

			frame.setVisible(false);

			System.out.println("Activer client");

		} else if (e.getSource().equals(retirerArgent)) {

			frame.setVisible(false);

			RetirerArgent retirerArgent = new RetirerArgent();
			retirerArgent.visuel();

		} else if (e.getSource().equals(activerCompte)) {

			frame.setVisible(false);

			System.out.println("Activer compte");

		} else if (e.getSource().equals(gererConseiller)) {

			frame.setVisible(false);

			GererConseiller gererConseiller = new GererConseiller();
			gererConseiller.visuel();

		} else if (e.getSource().equals(boutonRetour)) {

			frame.setVisible(false);

			AccueilLogin graph = new AccueilLogin();
			graph.visuel();
			
		}
	}
}
