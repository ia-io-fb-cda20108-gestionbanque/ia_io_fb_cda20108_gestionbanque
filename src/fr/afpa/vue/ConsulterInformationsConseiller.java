package fr.afpa.vue;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fr.afpa.control.Control;

public class ConsulterInformationsConseiller extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private ImageIcon icon;
	private JLabel infoLabel;

	private JPanel panelFrame;
	private JPanel panelBoutton;
	private JPanel infoPanel;

	private JTextArea allInfos;
	private Container content;
	private GridBagConstraints gbc;

	private JLabel nom;
	private JLabel nomConseiller;
	private JLabel mail;
	private JLabel mailConseiller;
	private JLabel code;
	private JLabel codeConseiller;
	private JLabel comptes;
	private JLabel comptesConseiller;
	private JButton boutonRetour;

	@SuppressWarnings("serial")
	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();

		gbc.insets = new Insets(30, 30, 30, 30);

		// Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		// Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(panelBoutton, gbc);

		// Titre
		infoLabel = new JLabel("Vos Informations");
		infoLabel.setFont(new Font("Century Gothic", Font.BOLD, 75));
		infoLabel.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 25;
		panelFrame.add(infoLabel, gbc);

		// Affichage infos
		allInfos = new JTextArea(5, 25);
		allInfos.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		allInfos.setForeground(new Color(37, 41, 58));
		allInfos.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		allInfos.setBackground(new Color(255, 255, 255));
		allInfos.setEditable(false);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 25;

		// Dans TextArea

		infoPanel = new JPanel(new GridLayout(5, 2));
		infoPanel.setBackground(new Color(0, 0, 0, 0));

		// Ligne d'info NOM
		nom = new JLabel("Nom/Prenom : ");
		nom.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(nom);

		nomConseiller = new JLabel(Control.getConseiller().getNom() + " " + Control.getConseiller().getPrenom());
		nomConseiller.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(nomConseiller);

		// Ligne d'info MAIL
		mail = new JLabel("E-mail : ");
		mail.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(mail);

		mailConseiller = new JLabel(Control.getConseiller().getEmail());
		mailConseiller.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(mailConseiller);

		// Ligne d'info CODE
		code = new JLabel("Login : ");
		code.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(code);

		codeConseiller = new JLabel(Control.getConseiller().getLogin());
		codeConseiller.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(codeConseiller);

		// Ligne d'info NBCOMPTE
		comptes = new JLabel("Nombre de compte(s) : ");
		comptes.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(comptes);

		comptesConseiller = new JLabel(String.valueOf(Control.getConseiller().getNbrComptes()));
		comptesConseiller.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(comptesConseiller);

		gbc.gridx = 0;
		gbc.gridy = 2;
		
		panelFrame.add(infoPanel, gbc);
		panelFrame.add(allInfos, gbc);
		
		boutonRetour = new JButton("Retour");
		boutonRetour.setPreferredSize(new Dimension(400, 75));
		boutonRetour.setFont(new Font("Century Gothic", Font.BOLD, 30));
		boutonRetour.setForeground(Color.white);
		boutonRetour.setBackground(new Color(37, 41, 58));
		boutonRetour.addActionListener(this);
		boutonRetour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gbc.gridx = 0;
		gbc.gridy = 3;
		panelFrame.add(boutonRetour, gbc);

		

		// Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(boutonRetour)) {

			frame.setVisible(false);

			MenuConseiller menuConseiller = new MenuConseiller();
			menuConseiller.visuel();
			
		}
		
	}
}
