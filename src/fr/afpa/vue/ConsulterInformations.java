package fr.afpa.vue;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fr.afpa.control.Control;

public class ConsulterInformations extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	private JFrame frame;
	private ImageIcon icon;
	private JLabel infoLabel;
	
	private JPanel panelFrame;
	private JPanel panelBoutton;
	private JPanel infoPanel;

	private JTextArea allInfos;
	private Container content;
	private GridBagConstraints gbc;

	private JLabel nom;
	private JLabel nomClient;
	private JLabel date;
	private JLabel dateClient;
	private JLabel mail;
	private JLabel mailClient;
	private JLabel code;
	private JLabel codeClient;
	private JLabel comptes;
	private JLabel comptesClient;
	private JButton boutonRetour;


	@SuppressWarnings("serial")
	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();

		gbc.insets = new Insets(30, 30, 30, 30);
		
		//Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		//Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(panelBoutton, gbc);
		
		// Titre
		infoLabel = new JLabel("Vos Informations");
		infoLabel.setFont(new Font("Century Gothic", Font.BOLD, 75));
		infoLabel.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 25;
		panelFrame.add(infoLabel, gbc);

		
		// Affichage infos
		allInfos = new JTextArea(5,25);
		allInfos.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		allInfos.setForeground(new Color(37, 41, 58));
		allInfos.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		allInfos.setBackground(new Color(255,255,255));
		allInfos.setEditable(false);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 25;
		
		
		// Dans TextArea
		
		infoPanel = new JPanel(new GridLayout(5, 2));
		infoPanel.setBackground(new Color(0, 0, 0, 0));

		// Ligne d'info NOM
		nom = new JLabel("Nom/Prenom : ");
		nom.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(nom);
		
		nomClient = new JLabel(Control.getClient().getNom() + " " + Control.getClient().getPrenom());
		nomClient.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(nomClient);

		// Ligne d'info DATE
		date = new JLabel("Date de naissance : ");
		date.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(date);

		dateClient = new JLabel(String.valueOf(Control.getClient().getDateNaiss()));
		dateClient.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(dateClient);
		
		// Ligne d'info MAIL
		mail = new JLabel("E-mail : ");
		mail.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(mail);

		mailClient = new JLabel(Control.getClient().getEmail());
		mailClient.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(mailClient);

		// Ligne d'info CODE
		code = new JLabel("Code Client : ");
		code.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(code);

		codeClient = new JLabel(Control.getClient().getCodeClient());
		codeClient.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(codeClient);

		// Ligne d'info NBCOMPTE
		comptes = new JLabel("Nombre de compte(s) : ");
		comptes.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(comptes);

		comptesClient = new JLabel(String.valueOf(Control.getClient().getNbrComptes()));
		comptesClient.setFont(new Font("Century Gothic", Font.ITALIC, 50));
		infoPanel.add(comptesClient);
		
		gbc.gridx = 0;
		gbc.gridy = 2;		
		
		panelFrame.add(infoPanel, gbc);
		panelFrame.add(allInfos, gbc);
		
		boutonRetour = new JButton("Retour");
		boutonRetour.setPreferredSize(new Dimension(400, 75));
		boutonRetour.setFont(new Font("Century Gothic", Font.BOLD, 30));
		boutonRetour.setForeground(Color.white);
		boutonRetour.setBackground(new Color(37, 41, 58));
		boutonRetour.addActionListener(this);
		boutonRetour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gbc.gridx = 0;
		gbc.gridy = 3;
		panelFrame.add(boutonRetour, gbc);
		
		
		// Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource().equals(boutonRetour)) {

			frame.setVisible(false);

			MenuAdmin menuAdmin = new MenuAdmin();
			menuAdmin.visuel();
			
		}
	}
}
