package fr.afpa.vue;

import java.awt.*;
import javax.swing.*;

public class ModifSupp extends JFrame {

	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private ImageIcon icon;

	private JPanel panelFrame;
	private JPanel panelBouton;
	private JPanel panelInfos;
	private JPanel panelCheck;

	private JTextField demandeAgence;
	private JTextField demandeClient;
	private JTextField demandeTypeCompte;
	private JTextField numeroCompte;
	
	private JButton boutonSupp;
	private JCheckBox desactiverCompte;

	private Container content;
	private GridBagConstraints gbc;
	private Dimension dim;
	private Font font;

	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);

		dim = new Dimension(400, 50);
		
		font = new Font("Century Gothic", Font.BOLD, 40);

		demandeAgence = new JTextField("Agence");
		demandeAgence.setPreferredSize(dim);
		demandeAgence.setFont(font);
		demandeAgence.setForeground(new Color(205, 205, 205));
		demandeAgence.setHorizontalAlignment(JTextField.CENTER);
		demandeAgence.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		demandeClient = new JTextField("Numero Client");
		demandeClient.setPreferredSize(dim);
		demandeClient.setFont(font);
		demandeClient.setForeground(new Color(205, 205, 205));
		demandeClient.setHorizontalAlignment(JTextField.CENTER);
		demandeClient.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		demandeTypeCompte = new JTextField("Type de compte");
		demandeTypeCompte.setPreferredSize(dim);
		demandeTypeCompte.setFont(font);
		demandeTypeCompte.setForeground(new Color(205, 205, 205));
		demandeTypeCompte.setHorizontalAlignment(JTextField.CENTER);
		demandeTypeCompte.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		numeroCompte = new JTextField("Numero du compte");
		numeroCompte.setPreferredSize(dim);
		numeroCompte.setFont(font);
		numeroCompte.setForeground(new Color(205, 205, 205));
		numeroCompte.setHorizontalAlignment(JTextField.CENTER);
		numeroCompte.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		desactiverCompte = new JCheckBox("Désactiver le compte");
		desactiverCompte.setBackground(new Color(0, 0, 0, 0));
		desactiverCompte.setFont(new Font("Century Gothic", Font.BOLD, 30));
		desactiverCompte.setForeground(new Color(37, 41, 58));

		boutonSupp = new JButton("Supprimer le compte");
		boutonSupp.setPreferredSize(new Dimension(400, 50));
		boutonSupp.setFont(new Font("Century Gothic", Font.BOLD, 30));
		boutonSupp.setForeground(Color.white);
		boutonSupp.setBackground(new Color(230, 0, 47));
		boutonSupp.setCursor(new Cursor(Cursor.HAND_CURSOR));
		
		panelCheck = new JPanel(new GridBagLayout());

		panelCheck.setBackground(new Color(0, 0, 0, 0));
		
		gbc.gridx = 1;
		gbc.gridy = 4;
		panelCheck.add(desactiverCompte, gbc);
		
		panelBouton = new JPanel(new GridBagLayout());
		
		panelBouton.setBackground(new Color(0, 0, 0, 0));
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelBouton.add(boutonSupp, gbc);
		
		panelInfos = new JPanel(new GridBagLayout());
		
		panelInfos.setBackground(new Color(0, 0, 0, 0));
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelInfos.add(demandeAgence, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		panelInfos.add(demandeClient, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		panelInfos.add(demandeTypeCompte, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		panelInfos.add(numeroCompte, gbc);

		// Panel regroupant le tout

		panelFrame = new JPanel(new GridBagLayout()) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		panelFrame.setBackground(new Color(0, 0, 0, 0));
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(panelInfos, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		panelFrame.add(panelCheck, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(panelBouton, gbc);

		// Affichage
		content = frame.getContentPane();
		content.add(panelFrame);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}
}