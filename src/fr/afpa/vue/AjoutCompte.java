package fr.afpa.vue;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fr.afpa.control.Control;

public class AjoutCompte extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private ImageIcon icon;

	private JPanel panelFrame;
	
	private JTextField demandeAgence;
	private JTextField demandeClient;
	private JTextField demandeTypeCompte;
	private JTextField demandeSoldeDepot;
	private JTextField numeroCompte;
	private JTextField decouvert;
	private JButton boutonCreer;
	
	private Container content;
	private GridBagConstraints gbc;
	private Dimension dim;
	private Font font;


	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);

		dim = new Dimension(400, 50);
		
		font = new Font("Century Gothic", Font.BOLD, 40);

		demandeAgence = new JTextField("Agence");
		demandeAgence.setPreferredSize(dim);
		demandeAgence.setFont(font);
		demandeAgence.setForeground(new Color(205, 205, 205));
		demandeAgence.setHorizontalAlignment(JTextField.CENTER);
		demandeAgence.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		demandeClient = new JTextField("Client");
		demandeClient.setPreferredSize(dim);
		demandeClient.setFont(font);
		demandeClient.setForeground(new Color(205, 205, 205));
		demandeClient.setHorizontalAlignment(JTextField.CENTER);
		demandeClient.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		demandeTypeCompte = new JTextField("Type de compte");
		demandeTypeCompte.setPreferredSize(dim);
		demandeTypeCompte.setFont(font);
		demandeTypeCompte.setForeground(new Color(205, 205, 205));
		demandeTypeCompte.setHorizontalAlignment(JTextField.CENTER);
		demandeTypeCompte.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		demandeSoldeDepot = new JTextField("Solde de depot");
		demandeSoldeDepot.setPreferredSize(dim);
		demandeSoldeDepot.setFont(font);
		demandeSoldeDepot.setForeground(new Color(205, 205, 205));
		demandeSoldeDepot.setHorizontalAlignment(JTextField.CENTER);
		demandeSoldeDepot.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		numeroCompte = new JTextField("Numero de compte");
		numeroCompte.setPreferredSize(dim);
		numeroCompte.setFont(font);
		numeroCompte.setForeground(new Color(205, 205, 205));
		numeroCompte.setHorizontalAlignment(JTextField.CENTER);
		numeroCompte.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		

		decouvert = new JTextField("Decouvert Y/N");
		decouvert.setPreferredSize(dim);
		decouvert.setFont(font);
		decouvert.setForeground(new Color(205, 205, 205));
		decouvert.setHorizontalAlignment(JTextField.CENTER);
		decouvert.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		boutonCreer = new JButton("Cr�er le compte");
		boutonCreer.setPreferredSize(dim);
		boutonCreer.setFont(new Font("Century Gothic", Font.BOLD, 30));
		boutonCreer.setForeground(Color.white);
		boutonCreer.setBackground(new Color(37, 41, 58));
		boutonCreer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		boutonCreer.addActionListener(this);

		// Panel regroupant le tout

		panelFrame = new JPanel(new GridBagLayout()) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		panelFrame.setBackground(new Color(0, 0, 0, 0));

		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(demandeAgence, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		panelFrame.add(demandeTypeCompte, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		panelFrame.add(demandeSoldeDepot, gbc);

		gbc.gridx = 1;
		gbc.gridy = 0;
		panelFrame.add(numeroCompte, gbc);
		

		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(decouvert, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 2;
		panelFrame.add(demandeClient, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.CENTER;
		panelFrame.add(boutonCreer, gbc);

		// Affichage
		content = frame.getContentPane();
		content.add(panelFrame);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(boutonCreer)) {
			Control.ajoutCompteBancaire(demandeAgence.getText(), 
					numeroCompte.getText(), 
					demandeSoldeDepot.getText(), 
					demandeTypeCompte.getText(), 
					demandeClient.getText(), 
					decouvert.getText());
		}
		
	}
}