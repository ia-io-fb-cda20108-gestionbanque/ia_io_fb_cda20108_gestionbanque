package fr.afpa.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MenuConseiller extends JFrame implements ActionListener {
	private JFrame frame;
	private ImageIcon icon;
	private JLabel titre;

	private JPanel panelFrame;
	private JPanel panelBoutton;

	private Container content;
	private GridBagConstraints gbc;
	private JButton consulterInfos;
	private JButton consulterComptes;
	private JButton consulterOperation;
	private JButton virement;
	private JButton imprimerReleve;
	private JButton alimenterCompte;
	private JButton retirerArgent;
	private JButton gererComptes;
	private JButton gererClient;
	private JButton changerDomiciliation;
	private JButton boutonRetour;

	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);

		// Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		// Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.CENTER;
		panelFrame.add(panelBoutton, gbc);

		// Titre
		titre = new JLabel("Menu Conseiller");
		titre.setFont(new Font("Century Gothic", Font.BOLD, 60));
		titre.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.CENTER;
		panelFrame.add(titre, gbc);

		// Button ConsulterInfos
		consulterInfos = new JButton("Consulter vos Informations");
		consulterInfos.setFont(new Font("Century Gothic", Font.BOLD, 30));
		consulterInfos.setForeground(new Color(37, 41, 58));
		consulterInfos.setBackground(Color.white);
		consulterInfos.setCursor(new Cursor(Cursor.HAND_CURSOR));
		consulterInfos.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panelFrame.add(consulterInfos, gbc);

		// Entrez ConsulterComptes
		consulterComptes = new JButton("Consulter vos Comptes");
		consulterComptes.setFont(new Font("Century Gothic", Font.BOLD, 30));
		consulterComptes.setForeground(new Color(37, 41, 58));
		consulterComptes.setBackground(Color.white);
		consulterComptes.setCursor(new Cursor(Cursor.HAND_CURSOR));
		consulterComptes.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(consulterComptes, gbc);

		// Entrez la ville
		consulterOperation = new JButton("Consulter vos Op�rations");
		consulterOperation.setFont(new Font("Century Gothic", Font.BOLD, 30));
		consulterOperation.setForeground(new Color(37, 41, 58));
		consulterOperation.setBackground(Color.white);
		consulterOperation.setCursor(new Cursor(Cursor.HAND_CURSOR));
		consulterOperation.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 3;
		panelFrame.add(consulterOperation, gbc);

		// Entrez la ville
		virement = new JButton("Faire un Virement");
		virement.setFont(new Font("Century Gothic", Font.BOLD, 30));
		virement.setForeground(new Color(37, 41, 58));
		virement.setBackground(Color.white);
		virement.setCursor(new Cursor(Cursor.HAND_CURSOR));
		virement.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelFrame.add(virement, gbc);

		// Entrez la ville
		imprimerReleve = new JButton("Imprimer un relev�");
		imprimerReleve.setFont(new Font("Century Gothic", Font.BOLD, 30));
		imprimerReleve.setForeground(new Color(37, 41, 58));
		imprimerReleve.setBackground(Color.white);
		imprimerReleve.setCursor(new Cursor(Cursor.HAND_CURSOR));
		imprimerReleve.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 5;
		panelFrame.add(imprimerReleve, gbc);

		// Entrez la ville
		alimenterCompte = new JButton("Alimenter Compte");
		alimenterCompte.setFont(new Font("Century Gothic", Font.BOLD, 30));
		alimenterCompte.setForeground(new Color(37, 41, 58));
		alimenterCompte.setBackground(Color.white);
		alimenterCompte.setCursor(new Cursor(Cursor.HAND_CURSOR));
		alimenterCompte.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 1;
		panelFrame.add(alimenterCompte, gbc);

		// Entrez la ville
		retirerArgent = new JButton("Retirer Argent");
		retirerArgent.setFont(new Font("Century Gothic", Font.BOLD, 30));
		retirerArgent.setForeground(new Color(37, 41, 58));
		retirerArgent.setBackground(Color.white);
		retirerArgent.setCursor(new Cursor(Cursor.HAND_CURSOR));
		retirerArgent.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 2;
		panelFrame.add(retirerArgent, gbc);

		// Entrez la ville
		gererComptes = new JButton("G�rer les comptes");
		gererComptes.setFont(new Font("Century Gothic", Font.BOLD, 30));
		gererComptes.setForeground(new Color(37, 41, 58));
		gererComptes.setBackground(Color.white);
		gererComptes.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gererComptes.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 3;
		panelFrame.add(gererComptes, gbc);

		// Entrez la ville
		gererClient = new JButton("G�rer les clients");
		gererClient.setFont(new Font("Century Gothic", Font.BOLD, 30));
		gererClient.setForeground(new Color(37, 41, 58));
		gererClient.setBackground(Color.white);
		gererClient.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gererClient.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 4;
		panelFrame.add(gererClient, gbc);

		// Entrez la ville
		changerDomiciliation = new JButton("Changer domiciliation client");
		changerDomiciliation.setFont(new Font("Century Gothic", Font.BOLD, 30));
		changerDomiciliation.setForeground(new Color(37, 41, 58));
		changerDomiciliation.setBackground(Color.white);
		changerDomiciliation.setCursor(new Cursor(Cursor.HAND_CURSOR));
		changerDomiciliation.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 5;
		panelFrame.add(changerDomiciliation, gbc);
		
		boutonRetour = new JButton("Retour");
		boutonRetour.setPreferredSize(new Dimension(300, 50));
		boutonRetour.setFont(new Font("Century Gothic", Font.BOLD, 30));
		boutonRetour.setForeground(Color.white);
		boutonRetour.setBackground(new Color(37, 41, 58));
		boutonRetour.addActionListener(this);
		boutonRetour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gbc.gridx = 1;
		gbc.gridy = 6;
		panelFrame.add(boutonRetour, gbc);

		// Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(consulterInfos)) {

			frame.setVisible(false);

			ConsulterInformationsConseiller info = new ConsulterInformationsConseiller();
			info.visuel();

		} else if (e.getSource().equals(alimenterCompte)) {

			frame.setVisible(false);

			AlimenterCompte alimenterCompte = new AlimenterCompte();
			alimenterCompte.visuel();

		} else if (e.getSource().equals(consulterComptes)) {

			frame.setVisible(false);

			VosComptes comptes = new VosComptes();
			comptes.visuel();

		} else if (e.getSource().equals(retirerArgent)) {

			frame.setVisible(false);

			RetirerArgent retirerArgent = new RetirerArgent();
			retirerArgent.visuel();

		} else if (e.getSource().equals(consulterOperation)) {

			frame.setVisible(false);

			Operations operation = new Operations();
			operation.visuel();

		} else if (e.getSource().equals(gererComptes)) {

			frame.setVisible(false);

			GererCompte gererCompte = new GererCompte();
			gererCompte.visuel();

		} else if (e.getSource().equals(virement)) {

			frame.setVisible(false);

			FaireVirement virement = new FaireVirement();
			virement.visuel();

		} else if (e.getSource().equals(gererClient)) {

			frame.setVisible(false);

			GererClient gererClient = new GererClient();
			gererClient.visuel();

		} else if (e.getSource().equals(imprimerReleve)) {

			frame.setVisible(false);

			System.out.println("PDF");

		} else if (e.getSource().equals(changerDomiciliation)) {

			frame.setVisible(false);

			ChangerDomiciliation changerDomiciliation = new ChangerDomiciliation();
			changerDomiciliation.visuel();

		} else if (e.getSource().equals(boutonRetour)) {

			frame.setVisible(false);

			AccueilLogin graph = new AccueilLogin();
			graph.visuel();
			
		}
	}
}
