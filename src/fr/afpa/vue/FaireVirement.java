package fr.afpa.vue;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fr.afpa.control.Control;

public class FaireVirement extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private ImageIcon icon;
	private JLabel entrezCompte;
	
	private JPanel panelFrame;
	private JPanel panelBoutton;
	
	private JTextField demandeCompteD;
	private JTextField demandeCompteC;
	private JTextField demandeDuMontant;
	
	private Container content;
	private GridBagConstraints gbc;
	private JButton valider, effacer;
	private JButton boutonRetour;

	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);
		
		//Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		//Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelFrame.add(panelBoutton, gbc);
		
		// Titre
		entrezCompte = new JLabel("Entrez le compte � d�biter");
		entrezCompte.setFont(new Font("Century Gothic", Font.BOLD, 60));
		entrezCompte.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(entrezCompte, gbc);

		// Entrez le num�ro de Compte � d�biter
		demandeCompteD = new JTextField("Num�ro du compte", 10);
		demandeCompteD.setFont(new Font("Century Gothic", Font.BOLD, 40));
		demandeCompteD.setHorizontalAlignment(JTextField.CENTER); 
		demandeCompteD.setForeground(new Color(205, 205, 205));
		demandeCompteD.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(demandeCompteD, gbc);
		
		boutonRetour = new JButton("Retour");
		boutonRetour.setPreferredSize(new Dimension(300, 50));
		boutonRetour.setFont(new Font("Century Gothic", Font.BOLD, 30));
		boutonRetour.setForeground(Color.white);
		boutonRetour.setBackground(new Color(37, 41, 58));
		boutonRetour.addActionListener(this);
		boutonRetour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gbc.gridx = 0;
		gbc.gridy = 5;
		panelFrame.add(boutonRetour, gbc);
		
		//Button Valider
		valider = new JButton("Valider");
		valider.setFont(new Font("Century Gothic", Font.BOLD, 30));
		valider.setForeground(Color.white);
		valider.setBackground(new Color(37, 41, 58));
		valider.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelBoutton.add(valider);

		//Button Effacer
		effacer = new JButton("Effacer");
		effacer.setFont(new Font("Century Gothic", Font.BOLD, 30));
		effacer.setForeground(new Color(37, 41, 58));
		effacer.setBackground(Color.white);
		effacer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelBoutton.add(effacer);
		
		//Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	public void visuelSuite1() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);
		
		//Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		//Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelFrame.add(panelBoutton, gbc);
		
		// Titre
		entrezCompte = new JLabel("Entrez le compte � cr�diter");
		entrezCompte.setFont(new Font("Century Gothic", Font.BOLD, 60));
		entrezCompte.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(entrezCompte, gbc);

		// Entrez le num�ro de Compte
		demandeCompteC = new JTextField("Num�ro du compte", 10);
		demandeCompteC.setFont(new Font("Century Gothic", Font.BOLD, 40));
		demandeCompteC.setHorizontalAlignment(JTextField.CENTER); 
		demandeCompteC.setForeground(new Color(205, 205, 205));
		demandeCompteC.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(demandeCompteC, gbc);
		
		//Button Valider
		valider = new JButton("Valider");
		valider.setFont(new Font("Century Gothic", Font.BOLD, 30));
		valider.setForeground(Color.white);
		valider.setBackground(new Color(37, 41, 58));
		valider.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelBoutton.add(valider);

		//Button Effacer
		effacer = new JButton("Effacer");
		effacer.setFont(new Font("Century Gothic", Font.BOLD, 30));
		effacer.setForeground(new Color(37, 41, 58));
		effacer.setBackground(Color.white);
		effacer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelBoutton.add(effacer);
		
		//Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	public void visuelSuite2() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);
		
		//Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		//Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelFrame.add(panelBoutton, gbc);
		
		// Titre
		entrezCompte = new JLabel("Entrez le montant � envoyer");
		entrezCompte.setFont(new Font("Century Gothic", Font.BOLD, 60));
		entrezCompte.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(entrezCompte, gbc);

		// Entrez le num�ro de Compte
		demandeDuMontant = new JTextField("Le montant en �", 10);
		demandeDuMontant.setFont(new Font("Century Gothic", Font.BOLD, 40));
		demandeDuMontant.setHorizontalAlignment(JTextField.CENTER); 
		demandeDuMontant.setForeground(new Color(205, 205, 205));
		demandeDuMontant.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(demandeDuMontant, gbc);
		
		//Button Valider
		valider = new JButton("Valider");
		valider.setFont(new Font("Century Gothic", Font.BOLD, 30));
		valider.setForeground(Color.white);
		valider.setBackground(new Color(37, 41, 58));
		valider.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelBoutton.add(valider);

		//Button Effacer
		effacer = new JButton("Effacer");
		effacer.setFont(new Font("Century Gothic", Font.BOLD, 30));
		effacer.setForeground(new Color(37, 41, 58));
		effacer.setBackground(Color.white);
		effacer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelBoutton.add(effacer);
		
		//Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		
		if (e.getSource().equals(boutonRetour) && Control.getConnected().equals(Control.getClientStr())) {

			frame.setVisible(false);

			MenuClient menuClient = new MenuClient();
			menuClient.visuel();
			
		} else if (e.getSource().equals(boutonRetour) && Control.getConnected().equals(Control.getConseillerStr())) {

			frame.setVisible(false);

			MenuConseiller menuConseiller = new MenuConseiller();
			menuConseiller.visuel();
			
		} else if (e.getSource().equals(boutonRetour) && Control.getConnected().equals(Control.getAdminStr())) {

			frame.setVisible(false);

			MenuAdmin menuAdmin = new MenuAdmin();
			menuAdmin.visuel();
			
		}
	}
}
