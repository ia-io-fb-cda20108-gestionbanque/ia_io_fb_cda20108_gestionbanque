package fr.afpa.vue;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fr.afpa.beans.Conseiller;
import fr.afpa.control.Control;

public class GererConseiller extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private ImageIcon icon;

	private JPanel panelFrame;

	private JButton boutonAjouterCompte;
	private JTextField demandeNumeroCompte;
	private JButton boutonValider;
	private JButton boutonEffacer;
	private JButton boutonRetour;

	private JPanel panelBouton;

	private Container content;
	private GridBagConstraints gbc;
	private Dimension dim;
	private Font font;
	
	private Conseiller verifIdConseiller;

	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);

		dim = new Dimension(300, 50);

		font = new Font("Century Gothic", Font.BOLD, 30);

		boutonAjouterCompte = new JButton("Ajouter un compte");
		boutonAjouterCompte.setPreferredSize(new Dimension(400, 50));
		boutonAjouterCompte.setFont(font);
		boutonAjouterCompte.setForeground(Color.white);
		boutonAjouterCompte.setBackground(new Color(37, 41, 58));
		boutonAjouterCompte.setCursor(new Cursor(Cursor.HAND_CURSOR));
		boutonAjouterCompte.addActionListener(this);

		demandeNumeroCompte = new JTextField("Login");
		demandeNumeroCompte.setPreferredSize(new Dimension(700, 50));
		demandeNumeroCompte.setFont(font);
		demandeNumeroCompte.setForeground(new Color(205, 205, 205));
		demandeNumeroCompte.setHorizontalAlignment(JTextField.CENTER);
		demandeNumeroCompte.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		boutonValider = new JButton("Valider");
		boutonValider.setPreferredSize(dim);
		boutonValider.setFont(font);
		boutonValider.setForeground(Color.white);
		boutonValider.setBackground(new Color(37, 41, 58));
		boutonValider.setCursor(new Cursor(Cursor.HAND_CURSOR));
		boutonValider.addActionListener(this);

		boutonEffacer = new JButton("Effacer");
		boutonEffacer.setPreferredSize(dim);
		boutonEffacer.setFont(font);
		boutonEffacer.setForeground(new Color(37, 41, 58));
		boutonEffacer.setBackground(Color.white);
		boutonEffacer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		boutonEffacer.addActionListener(this);

		panelBouton = new JPanel(new GridBagLayout());

		panelBouton.setBackground(new Color(0, 0, 0, 0));

		// Panel regroupant le tout

		panelFrame = new JPanel(new GridBagLayout()) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		panelFrame.setBackground(new Color(0, 0, 0, 0));

		gbc.gridx = 2;
		gbc.gridy = 0;
		panelFrame.add(boutonAjouterCompte, gbc);

		gbc.gridx = 1;
		gbc.gridy = 1;
		panelFrame.add(demandeNumeroCompte, gbc);

		gbc.gridx = 0;
		gbc.gridy = 0;
		panelBouton.add(boutonValider, gbc);

		gbc.gridx = 1;
		gbc.gridy = 0;
		panelBouton.add(boutonEffacer, gbc);

		gbc.gridx = 1;
		gbc.gridy = 2;
		panelFrame.add(panelBouton, gbc);
		
		boutonRetour = new JButton("Retour");
		boutonRetour.setPreferredSize(dim);
		boutonRetour.setFont(new Font("Century Gothic", Font.BOLD, 30));
		boutonRetour.setForeground(Color.white);
		boutonRetour.setBackground(new Color(37, 41, 58));
		boutonRetour.addActionListener(this);
		boutonRetour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gbc.gridx = 1;
		gbc.gridy = 3;
		
		panelFrame.add(boutonRetour, gbc);

		// Affichage
		content = frame.getContentPane();
		content.add(panelFrame);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	AjoutConseiller ajoutConseiller = new AjoutConseiller();
	ModifSuppConseiller modifConseiller = new ModifSuppConseiller();

	@Override
	public void actionPerformed(ActionEvent e) {
		
		verifIdConseiller = Control.controlIdConseiller(demandeNumeroCompte.getText());
		if (e.getSource().equals(boutonEffacer)) {
			demandeNumeroCompte.setText("");
		}

		if (e.getSource().equals(boutonAjouterCompte)) {
			frame.setVisible(false);
			ajoutConseiller.visuel();
		}
		if (e.getSource().equals(boutonValider) && verifIdConseiller != null){
			System.out.println("oue");
			frame.setVisible(false);
			modifConseiller.visuel(verifIdConseiller);
		}

		if (e.getSource().equals(boutonRetour)) {

			frame.setVisible(false);

			MenuAdmin menuAdmin = new MenuAdmin();
			menuAdmin.visuel();
			
		}
	}
}
