package fr.afpa.vue;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fr.afpa.control.Control;

public class CreerAgence extends JFrame implements ActionListener{

	private JFrame frame;
	private ImageIcon icon;
	private JLabel titre;
	
	private JPanel panelFrame;
	private JPanel panelBoutton;
	
	private JTextField numeroRue;
	private JTextField nomRue;
	private JTextField ville;
	private JTextField codeAgence;
	private JTextField nomAgence;
	private Container content;
	private GridBagConstraints gbc;
	private JButton creer;

	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);
		
		//Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		//Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.CENTER;
		panelFrame.add(panelBoutton, gbc);
		
		// Titre
		titre = new JLabel("Creer votre Agence");
		titre.setFont(new Font("Century Gothic", Font.BOLD, 60));
		titre.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.CENTER;
		panelFrame.add(titre, gbc);

		// Entrez le numero de rue
		numeroRue = new JTextField("Num�ro de rue", 10);
		numeroRue.setFont(new Font("Century Gothic",Font.BOLD, 40));
		numeroRue.setHorizontalAlignment(JTextField.CENTER); 
		numeroRue.setForeground(new Color(205, 205, 205));
		numeroRue.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		panelFrame.add(numeroRue, gbc);
		
		// Entrez la rue
		nomRue = new JTextField("Nom de rue", 10);
		nomRue.setFont(new Font("Century Gothic", Font.BOLD, 40));
		nomRue.setHorizontalAlignment(JTextField.CENTER); 
		nomRue.setForeground(new Color(205, 205, 205));
		nomRue.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(nomRue, gbc);

		// Entrez la ville
		ville = new JTextField("Ville", 10);
		ville.setFont(new Font("Century Gothic", Font.BOLD, 40));
		ville.setHorizontalAlignment(JTextField.CENTER); 
		ville.setForeground(new Color(205, 205, 205));
		ville.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 3;
		panelFrame.add(ville, gbc);
		

		// Entrez la ville
		codeAgence = new JTextField("Code Agence", 10);
		codeAgence.setFont(new Font("Century Gothic", Font.BOLD, 40));
		codeAgence.setHorizontalAlignment(JTextField.CENTER); 
		codeAgence.setForeground(new Color(205, 205, 205));
		codeAgence.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 1;
		gbc.gridy = 1;
		panelFrame.add(codeAgence, gbc);
		

		// Entrez la ville
		nomAgence = new JTextField("Nom Agence", 10);
		nomAgence.setFont(new Font("Century Gothic", Font.BOLD, 40));
		nomAgence.setHorizontalAlignment(JTextField.CENTER); 
		nomAgence.setForeground(new Color(205, 205, 205));
		nomAgence.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 1;
		gbc.gridy = 2;
		panelFrame.add(nomAgence, gbc);
		
		//Button Enregistrer
		creer = new JButton("Creer");
		creer.setFont(new Font("Century Gothic", Font.BOLD, 30));
		creer.setForeground(Color.white);
		creer.setBackground(new Color(37, 41, 58));
		creer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		creer.addActionListener(this);
		panelBoutton.add(creer);

		
		//Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		boolean verifFormat =  Control.creerAgence(
									codeAgence.getText(), 
									nomAgence.getText(),
									numeroRue.getText(), 
									nomRue.getText(),
									ville.getText()
									);
		if (verifFormat) {
			
		}else {
			//popup
		}
		
	}
}
