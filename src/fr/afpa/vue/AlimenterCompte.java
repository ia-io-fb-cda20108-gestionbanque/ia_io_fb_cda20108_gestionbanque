package fr.afpa.vue;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class AlimenterCompte extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private ImageIcon icon;
	private JLabel titre;
	
	private JPanel panelFrame;
	private JPanel panelBoutton;
	
	private JTextField demande;
	private Container content;
	private GridBagConstraints gbc;
	private JButton valider, effacer;

	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);
		
		//Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		//Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelFrame.add(panelBoutton, gbc);
		
		// Titre
		titre = new JLabel("Entrez le compte � alimenter");
		titre.setFont(new Font("Century Gothic", Font.BOLD, 60));
		titre.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(titre, gbc);

		// Entrez l ID
		demande = new JTextField("Num�ro du compte", 10);
		demande.setFont(new Font("Century Gothic", Font.BOLD, 40));
		demande.setHorizontalAlignment(JTextField.CENTER); 
		demande.setForeground(new Color(205, 205, 205));
		demande.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(demande, gbc);
		
		//Button Valider
		valider = new JButton("Valider");
		valider.setFont(new Font("Century Gothic", Font.BOLD, 30));
		valider.setForeground(Color.white);
		valider.setBackground(new Color(37, 41, 58));
		valider.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelBoutton.add(valider);

		//Button Effacer
		effacer = new JButton("Effacer");
		effacer.setFont(new Font("Century Gothic", Font.BOLD, 30));
		effacer.setForeground(new Color(37, 41, 58));
		effacer.setBackground(Color.white);
		effacer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelBoutton.add(effacer);
		
		//Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	public void visuelSuite1() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);
		
		//Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		//Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}
			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelFrame.add(panelBoutton, gbc);
		
		// Titre
		titre = new JLabel("Entrez le montant � ajouter");
		titre.setFont(new Font("Century Gothic", Font.BOLD, 60));
		titre.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(titre, gbc);

		// Entrez l ID
		demande = new JTextField("Le montant en �", 10);
		demande.setFont(new Font("Century Gothic", Font.BOLD, 40));
		demande.setHorizontalAlignment(JTextField.CENTER); 
		demande.setForeground(new Color(205, 205, 205));
		demande.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(demande, gbc);
		
		//Button Valider
		valider = new JButton("Valider");
		valider.setFont(new Font("Century Gothic", Font.BOLD, 30));
		valider.setForeground(Color.white);
		valider.setBackground(new Color(37, 41, 58));
		valider.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelBoutton.add(valider);

		//Button Effacer
		effacer = new JButton("Effacer");
		effacer.setFont(new Font("Century Gothic", Font.BOLD, 30));
		effacer.setForeground(new Color(37, 41, 58));
		effacer.setBackground(Color.white);
		effacer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelBoutton.add(effacer);
		
		//Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
	}
	
	

}
