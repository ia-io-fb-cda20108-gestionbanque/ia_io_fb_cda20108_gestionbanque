package fr.afpa.vue;

import java.awt.*;
import javax.swing.*;

public class DoubleAuthAdmin extends JFrame {

	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private ImageIcon icon;
	private JLabel cDABanque;

	private JPanel panelFrame;
	private JTextField demandeID;
	private Container content;
	private GridBagConstraints gbc;

	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);

		// Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		// Titre
		cDABanque = new JLabel("Entrez le code re�u par mail");
		cDABanque.setFont(new Font("Century Gothic", Font.BOLD, 75));
		cDABanque.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(cDABanque, gbc);

		// Entrez le code
		demandeID = new JTextField("Entrez code", 10);
		demandeID.setFont(new Font("Century Gothic", Font.BOLD, 50));
		demandeID.setHorizontalAlignment(JTextField.CENTER);
		demandeID.setForeground(new Color(205, 205, 205));
		demandeID.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(demandeID, gbc);

		// Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}
}