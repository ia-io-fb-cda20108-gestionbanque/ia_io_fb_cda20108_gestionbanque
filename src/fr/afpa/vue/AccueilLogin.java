package fr.afpa.vue;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fr.afpa.beans.*;
import fr.afpa.control.Control;

public class AccueilLogin extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private ImageIcon icon;
	private JLabel cDABanque;

	private JPanel panelFrame;
	private JPanel panelBoutton;

	private JTextField demandeID;
	private Container content;
	private GridBagConstraints gbc;
	private JButton valider, effacer;
	private Client verifIdClient;
	private Conseiller verifIdConseiller;
	private Admin verifIdAdmin;

	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);

		// Panel principale
		panelFrame = new JPanel(new GridBagLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		// Panel boutton
		panelBoutton = new JPanel(new FlowLayout()) {
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 500, 500, this);
				super.paintComponent(graphics);
			}
		};
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelFrame.add(panelBoutton, gbc);

		// Titre
		cDABanque = new JLabel("CDA BANQUE");
		cDABanque.setFont(new Font("Century Gothic", Font.BOLD, 75));
		cDABanque.setForeground(new Color(37, 41, 58));
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(cDABanque, gbc);

		// Entrez l ID
		demandeID = new JTextField("Entrez votre ID", 10);
		demandeID.setFont(new Font("Century Gothic", Font.BOLD, 50));
		demandeID.setHorizontalAlignment(JTextField.CENTER);
		demandeID.setForeground(new Color(205, 205, 205));
		demandeID.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(demandeID, gbc);

		// Button Valider
		valider = new JButton("Valider");
		valider.setFont(new Font("Century Gothic", Font.BOLD, 30));
		valider.setForeground(Color.white);
		valider.setBackground(new Color(37, 41, 58));
		valider.setCursor(new Cursor(Cursor.HAND_CURSOR));
		valider.addActionListener(this);
		panelBoutton.add(valider);

		// Button Effacer
		effacer = new JButton("Effacer");
		effacer.setFont(new Font("Century Gothic", Font.BOLD, 30));
		effacer.setForeground(new Color(37, 41, 58));
		effacer.setBackground(Color.white);
		effacer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		effacer.addActionListener(this);
		panelBoutton.add(effacer);

		// Affichage
		content = frame.getContentPane();
		content.add(panelFrame, BorderLayout.CENTER);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(effacer)) {
			demandeID.setText("");
		}

		if (e.getSource().equals(valider)) {
			verifIdClient = Control.controlIdClient(demandeID.getText());
			verifIdConseiller = Control.controlIdConseiller(demandeID.getText());
			verifIdAdmin = Control.controlIdAdmin(demandeID.getText());

			if (verifIdClient != null ) {
				frame.setVisible(false);
				
				System.out.println("ok");
				Control.setConnected(Control.getClientStr());
				MenuClient menuClient = new MenuClient();
				menuClient.visuel();
				
			}
			else if (verifIdConseiller != null){
				frame.setVisible(false);
				
				Control.setConnected(Control.getConseillerStr());
				MenuConseiller menuConseiller = new MenuConseiller();
				menuConseiller.visuel();
				
			}
			else if (verifIdAdmin != null){
				frame.setVisible(false);
				
				Control.setConnected(Control.getAdminStr());
				MenuAdmin menuAdmin = new MenuAdmin();
				menuAdmin.visuel();
				
			}
			else {
				// popup mauvais id
			}
		}
	}
}
