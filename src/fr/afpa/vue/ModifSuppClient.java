package fr.afpa.vue;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fr.afpa.beans.Client;
import fr.afpa.control.Control;

public class ModifSuppClient extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private ImageIcon icon;
	private static Client client;

	private JPanel panelFrame;
	private JPanel panelBouton;
	private JPanel panelInfos;
	private JPanel panelCheck;

	private JTextField demandeNom;
	private JTextField demandePrenom;
	private JTextField demandeEmail;
	private JTextField demandeLogin;
	private JTextField numeroClient;
	private JTextField dateNaissance;
	private JButton boutonModif;
	private JButton boutonSupp;
	private JCheckBox desactiverCompte;

	private Container content;
	private GridBagConstraints gbc;
	private Dimension dim;
	private Font font;

	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);

		dim = new Dimension(300, 50);
		
		font = new Font("Century Gothic", Font.BOLD, 30);

		demandeNom = new JTextField("Nom");
		demandeNom.setPreferredSize(dim);
		demandeNom.setFont(font);
		demandeNom.setForeground(new Color(205, 205, 205));
		demandeNom.setHorizontalAlignment(JTextField.CENTER);
		demandeNom.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		demandePrenom = new JTextField("Prenom");
		demandePrenom.setPreferredSize(dim);
		demandePrenom.setFont(font);
		demandePrenom.setForeground(new Color(205, 205, 205));
		demandePrenom.setHorizontalAlignment(JTextField.CENTER);
		demandePrenom.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		demandeEmail = new JTextField("Email");
		demandeEmail.setPreferredSize(dim);
		demandeEmail.setFont(font);
		demandeEmail.setForeground(new Color(205, 205, 205));
		demandeEmail.setHorizontalAlignment(JTextField.CENTER);
		demandeEmail.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		demandeLogin = new JTextField("Login");
		demandeLogin.setPreferredSize(dim);
		demandeLogin.setFont(font);
		demandeLogin.setForeground(new Color(205, 205, 205));
		demandeLogin.setHorizontalAlignment(JTextField.CENTER);
		demandeLogin.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		numeroClient = new JTextField("Numero Client");
		numeroClient.setPreferredSize(dim);
		numeroClient.setFont(font);
		numeroClient.setForeground(new Color(205, 205, 205));
		numeroClient.setHorizontalAlignment(JTextField.CENTER);
		numeroClient.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		dateNaissance = new JTextField("Date de naissance");
		dateNaissance.setPreferredSize(dim);
		dateNaissance.setFont(font);
		dateNaissance.setForeground(new Color(205, 205, 205));
		dateNaissance.setHorizontalAlignment(JTextField.CENTER);
		dateNaissance.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));

		desactiverCompte = new JCheckBox("Désactiver le compte");
		desactiverCompte.setBackground(new Color(0, 0, 0, 0));
		desactiverCompte.setFont(font);
		desactiverCompte.setForeground(new Color(37, 41, 58));

		boutonModif = new JButton("Modifier les informations");
		boutonModif.setPreferredSize(new Dimension(400, 50));
		boutonModif.setFont(font);
		boutonModif.setForeground(Color.white);
		boutonModif.setBackground(new Color(37, 41, 58));
		boutonModif.addActionListener(this);

		boutonSupp = new JButton("Supprimer le client");
		boutonSupp.setPreferredSize(new Dimension(400, 50));
		boutonSupp.setFont(font);
		boutonSupp.setForeground(Color.white);
		boutonSupp.setBackground(new Color(230, 0, 47));
		boutonSupp.setCursor(new Cursor(Cursor.HAND_CURSOR));
		boutonSupp.addActionListener(this);
		
		panelCheck = new JPanel(new GridBagLayout());

		panelCheck.setBackground(new Color(0, 0, 0, 0));
		
		gbc.gridx = 1;
		gbc.gridy = 4;
		panelCheck.add(desactiverCompte, gbc);
		
		panelBouton = new JPanel(new GridBagLayout());
		
		panelBouton.setBackground(new Color(0, 0, 0, 0));
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelBouton.add(boutonModif, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		panelBouton.add(boutonSupp, gbc);
		
		panelInfos = new JPanel(new GridBagLayout());
		
		panelInfos.setBackground(new Color(0, 0, 0, 0));
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelInfos.add(demandeNom, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		panelInfos.add(demandePrenom, gbc);

		gbc.gridx = 1;
		gbc.gridy = 2;
		panelInfos.add(demandeEmail, gbc);

		gbc.gridx = 1;
		gbc.gridy = 0;
		panelInfos.add(demandeLogin, gbc);

		gbc.gridx = 1;
		gbc.gridy = 1;
		panelInfos.add(numeroClient, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		panelInfos.add(dateNaissance, gbc);

		// Panel regroupant le tout

		panelFrame = new JPanel(new GridBagLayout()) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		panelFrame.setBackground(new Color(0, 0, 0, 0));
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(panelInfos, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		panelFrame.add(panelCheck, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelFrame.add(panelBouton, gbc);

		// Affichage
		content = frame.getContentPane();
		content.add(panelFrame);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(boutonModif)) {
			Control.modifierClient(demandeNom.getText(),
									demandePrenom.getText(),
									demandeEmail.getText(),
									demandeLogin.getText(),
									numeroClient.getText(),
									dateNaissance.getText()
									);
		}else if (e.getSource().equals(boutonSupp)) {
			Control.supprimerClient(client);
		}
		
	}

	public static Client getClient() {
		return client;
	}

	public static void setClient(Client client) {
		ModifSuppClient.client = client;
	}
}