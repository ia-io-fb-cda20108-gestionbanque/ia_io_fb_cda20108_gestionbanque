package fr.afpa.vue;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fr.afpa.control.Control;
import fr.afpa.metier.GestionAdmin;

public class AjoutConseiller extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private ImageIcon icon;

	private JPanel panelFrame;
	private JPanel panelInfos;
	private JPanel panelBouton;
	private JTextField demandeNom;
	private JTextField demandePrenom;
	private JTextField demandeLogin;
	private JTextField demandeEmail;
	private JButton boutonCreer;
	private Container content;
	private GridBagConstraints gbc;
	private Dimension dim;
	private JButton boutonRetour;
	

	public void visuel() {

		icon = new ImageIcon(".\\images\\fond.png");
		frame = new JFrame();
		gbc = new GridBagConstraints();
		gbc.insets = new Insets(20, 20, 20, 20);

		// Panel concernant les infos du client

		panelInfos = new JPanel(new GridBagLayout());

		panelInfos.setBackground(new Color(0, 0, 0, 0));

		dim = new Dimension(400, 75);

		demandeNom = new JTextField("Nom");
		demandeNom.setPreferredSize(dim);
		demandeNom.setFont(new Font("Century Gothic", Font.BOLD, 50));
		demandeNom.setForeground(new Color(205, 205, 205));
		demandeNom.setHorizontalAlignment(JTextField.CENTER);
		demandeNom.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelInfos.add(demandeNom, gbc);

		demandePrenom = new JTextField("Prenom");
		demandePrenom.setPreferredSize(dim);
		demandePrenom.setFont(new Font("Century Gothic", Font.BOLD, 50));
		demandePrenom.setForeground(new Color(205, 205, 205));
		demandePrenom.setHorizontalAlignment(JTextField.CENTER);
		demandePrenom.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 1;
		panelInfos.add(demandePrenom, gbc);

		demandeLogin = new JTextField("Login");
		demandeLogin.setPreferredSize(dim);
		demandeLogin.setFont(new Font("Century Gothic", Font.BOLD, 50));
		demandeLogin.setForeground(new Color(205, 205, 205));
		demandeLogin.setHorizontalAlignment(JTextField.CENTER);
		demandeLogin.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 3;
		panelInfos.add(demandeLogin, gbc);

		demandeEmail = new JTextField("Email");
		demandeEmail.setPreferredSize(dim);
		demandeEmail.setFont(new Font("Century Gothic", Font.BOLD, 50));
		demandeEmail.setForeground(new Color(205, 205, 205));
		demandeEmail.setHorizontalAlignment(JTextField.CENTER);
		demandeEmail.setBorder(BorderFactory.createLineBorder(new Color(37, 41, 58), 2));
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelInfos.add(demandeEmail, gbc);

		// Panel concernant le bouton pour cr�er

		panelBouton = new JPanel(new GridBagLayout());

		panelBouton.setBackground(new Color(0, 0, 0, 0));

		boutonCreer = new JButton("Cr�er");
		boutonCreer.setPreferredSize(dim);
		boutonCreer.setFont(new Font("Century Gothic", Font.BOLD, 30));
		boutonCreer.setForeground(Color.white);
		boutonCreer.setBackground(new Color(37, 41, 58));
		boutonCreer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		boutonCreer.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 0;

		panelBouton.add(boutonCreer, gbc);

		// Panel regroupant le tout

		panelFrame = new JPanel(new GridBagLayout()) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			Image img = icon.getImage();
			{
				setOpaque(false);
			}

			public void paintComponent(Graphics graphics) {
				graphics.drawImage(img, 0, 0, this);
				super.paintComponent(graphics);
			}
		};

		panelFrame.setBackground(new Color(0, 0, 0, 0));
		gbc.gridx = 0;
		gbc.gridy = 0;
		panelFrame.add(panelInfos, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		panelFrame.add(panelBouton, gbc);
		
		boutonRetour = new JButton("Retour");
		boutonRetour.setPreferredSize(dim);
		boutonRetour.setFont(new Font("Century Gothic", Font.BOLD, 30));
		boutonRetour.setForeground(Color.white);
		boutonRetour.setBackground(new Color(37, 41, 58));
		boutonRetour.addActionListener(this);
		boutonRetour.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gbc.gridx = 0;
		gbc.gridy = 5;
		panelFrame.add(boutonRetour, gbc);

		// Affichage
		content = frame.getContentPane();
		content.add(panelFrame);
		frame.pack();
		frame.add(panelFrame);
		frame.setSize(1400, 1000);
		frame.setLocation(300, 30);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource().equals(boutonCreer)) {
			boolean verifFormat = Control.ajoutConseiller(
					demandeNom.getText(), 
					demandePrenom.getText(),
					demandeEmail.getText(), 
					demandeLogin.getText());
			if (verifFormat) {
				// renvoie d'un popup
			}
		} else if (e.getSource().equals(boutonRetour)) {

			frame.setVisible(false);

			GererConseiller gererConseiller = new GererConseiller();
			gererConseiller.visuel();
			
		}

	}

}