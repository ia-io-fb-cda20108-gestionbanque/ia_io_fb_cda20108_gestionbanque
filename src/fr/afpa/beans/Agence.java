package fr.afpa.beans;

import java.io.Serializable;

public class Agence implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -370882966202943086L;
	private int code;
	private String nom;
	private Adresse adresse;
	
	public Agence(int code, String nom, Adresse adresse) {
		
		this.code = code;
		this.nom = nom;
		this.adresse = adresse;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}
	
	
	
}
