package fr.afpa.beans;

import java.io.Serializable;

public class Adresse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -516490206933676269L;
	private String numeroRue;
	private String nomRue;
	private String ville;
	
	public Adresse(String numeroRue, String nomRue, String ville) {

		this.numeroRue = numeroRue;
		this.nomRue = nomRue;
		this.ville = ville;
		
	}

	public String getNumeroRue() {
		return numeroRue;
	}

	public void setNumeroRue(String numeroRue) {
		this.numeroRue = numeroRue;
	}

	public String getNomRue() {
		return nomRue;
	}

	public void setNomRue(String nomRue) {
		this.nomRue = nomRue;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	
	
}
