package fr.afpa.beans;

import java.io.Serializable;

public class Conseiller extends Personne implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4276443782076014254L;

	public Conseiller(String nom, String prenom, String email, String login) {
		super(nom, prenom, email, login);
	}
	
	public Conseiller() {
		
	}
	
	
}
