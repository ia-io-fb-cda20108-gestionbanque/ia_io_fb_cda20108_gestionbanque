package fr.afpa.beans;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class Personne implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6382023531232822502L;
	private String nom;
	private String prenom;
	private String email;
	private String login;
	private ArrayList<CompteBancaire> compteBancaires;
	private int nbrComptes;
	
	public Personne(String nom, String prenom, String email, String login) {
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.login = login;
		this.nbrComptes = compteBancaires.size();
	}
	
	public Personne() {}

	
	public boolean ajoutCompteBancaire(CompteBancaire compte) {
		if (this.compteBancaires.size() <= 3) {			
			this.compteBancaires.add(compte);
			return true;
		}else {
			return false;
		}
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public ArrayList<CompteBancaire> getCompteBancaires() {
		return compteBancaires;
	}

	public void setCompteBancaires(ArrayList<CompteBancaire> compteBancaires) {
		this.compteBancaires = compteBancaires;
	}
	

	public int getNbrComptes() {
		return nbrComptes;
	}

	public void setNbrComptes(int nbrComptes) {
		this.nbrComptes = nbrComptes;
	}
	
	
	
}
