package fr.afpa.beans;

import java.io.Serializable;

public class CompteBancaire implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6285285838127419335L;
	private String numeroCompte;
	private float solde;
	private boolean decouvert;
	private float fraisTenue;
	private String typeCompte;
	private Agence agence;
	
	public CompteBancaire(String numeroCompte, float solde, boolean decouvert, Agence agence) {
		
		this.numeroCompte = numeroCompte;
		this.agence = agence;
		this.solde = solde;
		this.decouvert = decouvert;
		this.fraisTenue = choixTypeCompte();
		
	}
	
	public CompteBancaire() {}
	
	public float choixTypeCompte() {
		
		//A modifier (menu)
		
		if (decouvert) {
			//Calcul frais
			this.typeCompte = "Compte Courant";
			return 25;
			
		} else if (decouvert) {
			//Calcul frais
			this.typeCompte = "Livret A";
			return 1;
			
		} else if (decouvert) {
			//Calcul frais
			this.typeCompte = "Plan Epargne Logement";
			return 1;
		}
		
		return 0;
	}

	public String getNumeroCompte() {
		return numeroCompte;
	}

	public void setNumeroCompte(String numeroCompte) {
		this.numeroCompte = numeroCompte;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public boolean isDecouvert() {
		return decouvert;
	}

	public void setDecouvert(boolean decouvert) {
		this.decouvert = decouvert;
	}

	public float getFraisTenue() {
		return fraisTenue;
	}

	public void setFraisTenue(float fraisTenue) {
		this.fraisTenue = fraisTenue;
	}

	public String getTypeCompte() {
		return typeCompte;
	}

	public void setTypeCompte(String typeCompte) {
		this.typeCompte = typeCompte;
	}

	@Override
	public String toString() {
		return numeroCompte + ", solde : " + solde + ", decouvert : " + decouvert
				+ ", frais : " + fraisTenue + ", type : " + typeCompte;
	}

	public Agence getAgence() {
		return agence;
	}

	public void setAgence(Agence agence) {
		this.agence = agence;
	}
	
	
	
}
