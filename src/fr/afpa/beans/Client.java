package fr.afpa.beans;

import java.io.Serializable;
import java.time.LocalDate;

public class Client extends Personne implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3412540993261855749L;
	private String codeClient;
	private LocalDate dateNaiss;
	
	public Client(String nom, String prenom, String email, String login, String codeClient, LocalDate dateNaiss) {
		super(nom, prenom, email, login);
		this.codeClient = codeClient;
		this.dateNaiss = dateNaiss;
	}
	
	public Client() {}

	public String getCodeClient() {
		return codeClient;
	}

	public void setCodeClient(String codeClient) {
		this.codeClient = codeClient;
	}

	public LocalDate getDateNaiss() {
		return dateNaiss;
	}

	public void setDateNaiss(LocalDate dateNaiss) {
		this.dateNaiss = dateNaiss;
	}

}
