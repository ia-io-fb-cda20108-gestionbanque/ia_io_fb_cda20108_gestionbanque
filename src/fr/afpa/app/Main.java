package fr.afpa.app;

import java.io.IOException;

import fr.afpa.metier.GestionClient;
import fr.afpa.vue.*;

public class Main {

	public static void main(String[] args) throws IOException {

		
		//Control.lancementApp();
		
	
		AccueilLogin graph = new AccueilLogin();
		graph.visuel();
		
		FaireVirement virement = new FaireVirement();
		// virement.visuel();
		// virement.visuelSuite1();
		// virement.visuelSuite2();

		AlimenterCompte alimenterCompte = new AlimenterCompte();
		//alimenterCompte.visuel();
		// alimenterCompte.visuelSuite1();

		RetirerArgent retirerArgent = new RetirerArgent();
		// retirerArgent.visuel();
		// retirerArgent.visuelSuite1();

		CreerAgence creerAgence = new CreerAgence();
		//creerAgence.visuel();

		ChangerDomiciliation changerDomiciliation = new ChangerDomiciliation();
		// changerDomiciliation.visuel();

		ConsulterInformations info = new ConsulterInformations();
		//info.visuel();

		DoubleAuthAdmin doubleAuthAdmin = new DoubleAuthAdmin();
		// doubleAuthAdmin.visuel();

		VosComptes comptes = new VosComptes();
		//comptes.visuel();

		Operations operation = new Operations();
		// operation.visuel();

		ChaqueOperation chaqueOp = new ChaqueOperation();
		// chaqueOp.visuel();

		AjoutCompte ajoutCompte = new AjoutCompte();
		//ajoutCompte.visuel();

		GererCompte gererCompte = new GererCompte();
		//gererCompte.visuel();

		ModifSupp modifSupp = new ModifSupp();
		//modifSupp.visuel();

		GererClient gererClient = new GererClient();
		//gererClient.visuel();
		
		AjoutClient ajoutClient = new AjoutClient();
		//ajoutClient.visuel();

		ModifSuppClient modifSuppClient = new ModifSuppClient();
		//modifSuppClient.visuel(); 

		AjoutConseiller ajoutConseiller = new AjoutConseiller();
		//ajoutConseiller.visuel();

		GererConseiller gererConseiller = new GererConseiller();
		//gererConseiller.visuel();

		ModifSuppConseiller modifSuppConseiller = new ModifSuppConseiller();
		//modifSuppConseiller.visuel();

		MenuClient menuClient = new MenuClient();
		//menuClient.visuel();

		MenuConseiller menuConseiller = new MenuConseiller();
		//menuConseiller.visuel();

		MenuAdmin menuAdmin = new MenuAdmin();
		//menuAdmin.visuel();
		
		GestionClient gestionClient = new GestionClient();
		//gestionClient.imprimeReleve();

	}
}
