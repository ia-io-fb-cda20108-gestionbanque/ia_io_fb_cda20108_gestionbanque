package fr.afpa.metier;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import fr.afpa.beans.*;

public final class GestionAdmin extends GestionConseiller {

	private static String ADMIN_PATH = ".\\admin.data";

	public GestionAdmin() {

	}

	public static void creerAgence(String code, String nom, String numRue, String nomRue, String ville) {

		Adresse adresse = new Adresse(numRue, nomRue, ville);
		Agence agence = new Agence(Integer.valueOf(code), nom, adresse);
		ArrayList<Agence> listeAgence = lireFichierAgence();
		listeAgence.add(agence);
		ecrireFichier(agence, GestionClient.getAgencePath());

	}

	public static void ajoutConseiller(String nom, String prenom, String email, String login) {

		Conseiller conseiller = new Conseiller(nom, prenom, email, login);
		ArrayList<Conseiller> listeConseiller = lireFichierConseiller();
		listeConseiller.add(conseiller);
		ecrireFichier(listeConseiller, GestionConseiller.getConseillerPath());

		/*
		 * Deserialisation de la liste Si ma liste est vide ou j'ai eu une exc lors de
		 * la deser ==> Cr�e une nouvelle lisyte et j'ajoute mon conseiller et je ser la
		 * liste Si non ==> J'ajoute mon copnseiller et je ser
		 */

	}

	public void activerCompte() {

	}

	public void desactiverCompte() {

	}

	public void activerClient() {

	}

	public void desactiverClient() {

	}

	public static Client ajoutCompteBancaire(String numeroCompte, Client client, Agence agence, String solde, String decouvert) {
		boolean decouv = true;
		if (decouvert.equals("Y")) {
			decouv = true;
		}else {
			decouv = false;
		}
		CompteBancaire compte = new CompteBancaire(numeroCompte, Float.parseFloat(solde), decouv, agence);
		client.ajoutCompteBancaire(compte);
		return client;
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<Conseiller> lireFichierConseiller() {
		FileInputStream f = null;
		ObjectInputStream ois = null;
		ArrayList<Conseiller> list = new ArrayList<Conseiller>();

		try {

			f = new FileInputStream(GestionAdmin.getConseillerPath());
			ois = new ObjectInputStream(f);

			list = (ArrayList<Conseiller>) ois.readObject();

			ois.close();
			f.close();

		} catch (EOFException e) {
			try {
				ois.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				f.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			if (ois != null) {
				try {
					ois.close();
					f.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public static ArrayList<Admin> lireFichierAdmin() {
		FileInputStream f = null;
		ObjectInputStream ois = null;
		ArrayList<Admin> list = new ArrayList<Admin>();

		try {

			f = new FileInputStream(GestionAdmin.ADMIN_PATH);
			ois = new ObjectInputStream(f);

			list = (ArrayList<Admin>) ois.readObject();

			ois.close();
			f.close();

		} catch (EOFException e) {
			try {
				ois.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				f.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			if (ois != null) {
				try {
					ois.close();
					f.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public static ArrayList<Client> lireFichierClient() {
		FileInputStream f = null;
		ObjectInputStream ois = null;
		ArrayList<Client> list = new ArrayList<Client>();

		try {

			f = new FileInputStream(GestionAdmin.getClientPath());
			ois = new ObjectInputStream(f);

			list = (ArrayList<Client>) ois.readObject();

			ois.close();
			f.close();

		} catch (EOFException e) {
			try {
				ois.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				f.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			if (ois != null) {
				try {
					ois.close();
					f.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public static ArrayList<Agence> lireFichierAgence() {
		FileInputStream f = null;
		ObjectInputStream ois = null;
		ArrayList<Agence> list = new ArrayList<Agence>();

		try {

			f = new FileInputStream(GestionAdmin.getAgencePath());
			ois = new ObjectInputStream(f);

			list = (ArrayList<Agence>) ois.readObject();

			ois.close();
			f.close();

		} catch (ClassCastException e) {
			try {
				ois.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				f.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			if (ois != null) {
				try {
					ois.close();
					f.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	
	@SuppressWarnings("unchecked")
	public static ArrayList<CompteBancaire> lireFichierCompteBancaire() {
		FileInputStream f = null;
		ObjectInputStream ois = null;
		ArrayList<CompteBancaire> list = new ArrayList<CompteBancaire>();

		try {

			f = new FileInputStream(GestionAdmin.getCompteBancairePath());
			ois = new ObjectInputStream(f);

			list = (ArrayList<CompteBancaire>) ois.readObject();

			ois.close();
			f.close();

		} catch (EOFException e) {
			try {
				ois.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				f.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			if (ois != null) {
				try {
					ois.close();
					f.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	public static void ecrireFichier(Object objet, String path) {

		FileOutputStream f = null;
		ObjectOutputStream oos = null;

		try {

			f = new FileOutputStream(path);
			oos = new ObjectOutputStream(f);

			oos.writeObject(objet);
			oos.close();
			f.close();

		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			if (oos != null) {
				try {
					oos.close();
					f.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static String getAdminPath() {
		return ADMIN_PATH;
	}

	public static void ajouterAdmin(String nom, String prenom, String email, String login) {

		Admin admin = new Admin(nom, prenom, email, login);

		System.out.println("Ok !");

		ArrayList<Admin> listeAdmin = lireFichierAdmin();
		listeAdmin.add(admin);

		GestionAdmin.ecrireFichier(listeAdmin, GestionAdmin.getAdminPath());

	}

	public static void modifierAdmin(String nom, String prenom, String email, String login) {

		Admin admin = new Admin(nom, prenom, email, login);

		ArrayList<Admin> listeAdmin = lireFichierAdmin();

		for (int i = 0; i < listeAdmin.size(); i++) {
			if (listeAdmin.get(i).getLogin().equals(login)) {
				listeAdmin.remove(i);
				listeAdmin.add(admin);
			}
		}

		ecrireFichier(listeAdmin, getAdminPath());

	}

	public static void supprimerAdmin(String nom, String prenom, String email, String login) {

		ArrayList<Admin> listeAdmin = lireFichierAdmin();

		for (int i = 0; i < listeAdmin.size(); i++) {
			if (listeAdmin.get(i).getLogin().equals(login)) {
				listeAdmin.remove(i);
			}
		}

		ecrireFichier(listeAdmin, getAdminPath());

	}
}