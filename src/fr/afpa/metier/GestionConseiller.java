package fr.afpa.metier;

import java.time.LocalDate;
import java.util.ArrayList;

import fr.afpa.beans.*;
import fr.afpa.control.Control;

public class GestionConseiller extends GestionClient{

	private static final String CONSEILLER_PATH = ".\\conseiller.data";
	
	
	public GestionConseiller() {
		
	}
	
	public void gererCompte() {
		
	}
	
	public void gererClient() {
		
	}
	
	public static void ajoutClient(String nom, String prenom, String email, String login, String codeClient, String dateNaiss) {
		Client client = new Client(nom, prenom, email, login, codeClient, LocalDate.parse(dateNaiss));

		System.out.println("ok");
		
		ArrayList<Client> listeClient = GestionAdmin.lireFichierClient();
		listeClient.add(client);
		GestionAdmin.ecrireFichier(listeClient, GestionClient.getClientPath());
		
	}
	
	
	public static void modifierDomiciliationClient(String numRue, String nomRue, String ville, ArrayList<Agence> listAgence) {
		listAgence.get(Control.getVerifCodeAgence()).getAdresse().setNumeroRue(numRue);
		listAgence.get(Control.getVerifCodeAgence()).getAdresse().setNomRue(nomRue);
		listAgence.get(Control.getVerifCodeAgence()).getAdresse().setVille(ville);
		
		GestionAdmin.ecrireFichier(listAgence, GestionClient.getAgencePath());
		
		
	}

	public static String getConseillerPath() {
		return CONSEILLER_PATH;
	}

	public static void modifierClient(String nom, String prenom, String email, String login, String codeClient,
			String dateNaiss) {
		

		Client client = new Client(nom, prenom, email, login, codeClient, LocalDate.parse(dateNaiss));

		ArrayList<Client> listeClient = GestionAdmin.lireFichierClient();

		
		for (int i = 0; i < listeClient.size(); i++) {
			if (listeClient.get(i).getLogin().equals(login)) {
				listeClient.remove(i);
				listeClient.add(client);
			}
		}
		
		
		GestionAdmin.ecrireFichier(listeClient, GestionClient.getClientPath());
		
	}

	public static void modifierConseiller(String nom, String prenom, String email, String login) {

		Conseiller conseiller = new Conseiller(nom, prenom, email, login);

		ArrayList<Conseiller> listeConseiller = GestionAdmin.lireFichierConseiller();

		
		for (int i = 0; i < listeConseiller.size(); i++) {
			if (listeConseiller.get(i).getLogin().equals(login)) {
				listeConseiller.remove(i);
				listeConseiller.add(conseiller);
			}
		}
		GestionAdmin.ecrireFichier(listeConseiller, CONSEILLER_PATH);
	}

	public static void supprimerConseiller(Conseiller conseiller) {

		ArrayList<Conseiller> listeConseiller = GestionAdmin.lireFichierConseiller();

		
		for (int i = 0; i < listeConseiller.size(); i++) {
			if (listeConseiller.get(i).getLogin().equals(conseiller.getLogin())) {
				listeConseiller.remove(i);
			}
		}
		GestionAdmin.ecrireFichier(listeConseiller, CONSEILLER_PATH);
	}

}
