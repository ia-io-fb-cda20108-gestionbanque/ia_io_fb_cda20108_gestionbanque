package fr.afpa.metier;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

/*import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;*/

import fr.afpa.beans.Client;
import fr.afpa.control.Control;

public class GestionClient {

	private static final String CLIENT_PATH = ".\\client.data";
	private static final String AGENCE_PATH = ".\\agence.data";
	private static final String COMPTE_BANCAIRE_PATH = ".\\comptesBancaire.data";
	private static final String PDF_PATH = ".\\releve.pdf";

	public GestionClient() {

	}

	public void consulterInformation() {

	}

	public void consulterCompte() {

	}

	public void consulterOperationCompte() {

	}

	public void faireVirement() {

	}

	public void ajouterArgent() {

	}

	public void retirerArgent() {

	}

	/*public void imprimeReleve() {

		PdfWriter writer = null;
		PdfDocument pdf = null;
		Document pdfDoc = null;

		try {

			writer = new PdfWriter(PDF_PATH);
			pdf = new PdfDocument(writer);
			pdfDoc = new Document(pdf);

			Paragraph titre = new Paragraph("CDA Banque - Fiche Client").setFontSize(10f).setBold()
					.setTextAlignment(TextAlignment.CENTER);

			Paragraph numeroClient = new Paragraph("Numero Client : " + Control.getClient().getCodeClient())
					.setFont(PdfFontFactory.createFont(StandardFonts.HELVETICA)).setTextAlignment(TextAlignment.LEFT);
			Paragraph nom = new Paragraph("Nom : " + Control.getClient().getNom())
					.setFont(PdfFontFactory.createFont(StandardFonts.HELVETICA)).setTextAlignment(TextAlignment.LEFT);
			Paragraph prenom = new Paragraph("Prenom : " + Control.getClient().getPrenom())
					.setFont(PdfFontFactory.createFont(StandardFonts.HELVETICA)).setTextAlignment(TextAlignment.LEFT);
			Paragraph dateNaiss = new Paragraph("Date de naissance : " + Control.getClient().getDateNaiss())
					.setFont(PdfFontFactory.createFont(StandardFonts.HELVETICA)).setTextAlignment(TextAlignment.LEFT);

			Paragraph separation = new Paragraph(
					"______________________________________________________________________________")
							.setFont(PdfFontFactory.createFont(StandardFonts.HELVETICA))
							.setTextAlignment(TextAlignment.LEFT);

			pdfDoc.add(titre).add(numeroClient).add(nom).add(prenom).add(dateNaiss).add(separation);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();

		} finally {

			if (pdfDoc != null) {
				pdfDoc.close();
			}
		}
	}*/

	public static void supprimerClient(Client client) {

		ArrayList<Client> listeClient = GestionAdmin.lireFichierClient();

		for (int i = 0; i < listeClient.size(); i++) {
			if (listeClient.get(i).getLogin().equals(client.getLogin())) {
				listeClient.remove(i);
			}
		}

		GestionAdmin.ecrireFichier(listeClient, GestionClient.getClientPath());

	}

	// Getter And Setter
	public static String getClientPath() {
		return CLIENT_PATH;
	}

	public static String getAgencePath() {
		return AGENCE_PATH;
	}

	public static String getCompteBancairePath() {
		return COMPTE_BANCAIRE_PATH;
	}

}
